<?php

namespace App\Rules;

use App\Models\Tour;
use App\Models\Travel;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Translation\PotentiallyTranslatedString;

readonly class TourUniqRule implements ValidationRule
{
    /**
     * @param Closure(string): PotentiallyTranslatedString $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        /**@var Travel $travel*/
        $travel = request()
            ->route()
            ?->parameter('travel');
        /**@var Tour $tour*/
        $tour = request()
            ->route()
            ?->parameter('tour');
        if (!$travel) {
            return;
        }
        $query = $travel->tours()->where($attribute, $value);
        if ($tour) {
            $query->where('id', '<>', $tour->id);
        }
        if ($query->doesntExist()) {
            return;
        }
        $fail(trans('error.uniq_tour', ['attribute' => $attribute, 'value' => $value, 'travel' => $travel->name]));
    }
}
