<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class SomethingWentWrongException extends Exception implements Throwable
{
    public function __construct()
    {
        parent::__construct(trans('error.something_went_wrong'));
    }
}
