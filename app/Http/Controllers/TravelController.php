<?php

namespace App\Http\Controllers;

use App\Exceptions\SomethingWentWrongException;
use App\Helpers\Swagger\Enums\SwaggerFormatEnum;
use App\Helpers\Swagger\Enums\SwaggerInEnum;
use App\Helpers\Swagger\Enums\SwaggerTypeEnum;
use App\Http\Requests\TravelRequest;
use App\Http\Resources\ErrorResource;
use App\Http\Resources\TravelResource;
use App\Models\Travel;
use App\Services\TravelService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use OpenApi\Attributes as OAT;

class TravelController extends Controller
{
    #[
        OAT\Get(
            path: '/travel/{travelId}',
            operationId: 'TravelShow',
            summary: 'Travel show',
            tags: ['Travel'],
            parameters: [
                new OAT\Parameter(ref: '#/components/parameters/Content-Type'),
                new OAT\Parameter(ref: '#/components/parameters/Accept'),
                new OAT\Parameter(ref: '#/components/parameters/Authorization'),
                new OAT\PathParameter(
                    parameter: 'travelId',
                    name: 'travelId',
                    description: 'Travel ID',
                    in: SwaggerInEnum::PATH,
                    required: true,
                    schema: new OAT\Schema(
                        description: 'Travel ID',
                        type: SwaggerTypeEnum::STRING,
                        format: SwaggerFormatEnum::UUID,
                        example: '4f4bd032-e7d4-402a-bdf6-aaf6be240d53',
                        nullable: false,
                    ),
                )
            ],
            responses: [
                new OAT\Response(
                    response: Response::HTTP_OK,
                    description: 'Travel info',
                    content: new OAT\JsonContent(
                        properties: [
                            new OAT\Property(
                                property: 'data',
                                type: SwaggerTypeEnum::OBJECT,
                                allOf: [
                                    new OAT\Schema(ref: '#/components/schemas/TravelResource')
                                ]
                            ),
                        ],
                    ),
                ),
                new OAT\Response(
                    response: Response::HTTP_UNAUTHORIZED,
                    description: 'Unauthorized',
                    content: new OAT\JsonContent(ref: '#/components/schemas/ErrorResponse')
                ),
                new OAT\Response(
                    response: Response::HTTP_NOT_FOUND,
                    description: 'Travel not found',
                ),
            ],
            deprecated: false,
        )
    ]
    public function show(Travel $travel): TravelResource
    {
        return TravelResource::make($travel);
    }

    #[
        OAT\Post(
            path: '/travel',
            operationId: 'TravelStore',
            summary: 'Travel store',
            requestBody: new OAT\RequestBody(
                content: new OAT\JsonContent(
                    ref: '#/components/schemas/TravelRequest'
                )
            ),
            tags: ['Travel'],
            parameters: [
                new OAT\Parameter(ref: '#/components/parameters/Content-Type'),
                new OAT\Parameter(ref: '#/components/parameters/Accept'),
                new OAT\Parameter(ref: '#/components/parameters/Authorization'),
            ],
            responses: [
                new OAT\Response(
                    response: Response::HTTP_CREATED,
                    description: 'Travel info',
                    content: new OAT\JsonContent(
                        properties: [
                            new OAT\Property(
                                property: 'data',
                                type: SwaggerTypeEnum::OBJECT,
                                allOf: [
                                    new OAT\Schema(ref: '#/components/schemas/TravelResource')
                                ]
                            ),
                        ],
                    ),
                ),
                new OAT\Response(
                    response: Response::HTTP_BAD_REQUEST,
                    description: 'Something went wrong',
                    content: new OAT\JsonContent(ref: '#/components/schemas/ErrorResponse')
                ),
                new OAT\Response(
                    response: Response::HTTP_UNAUTHORIZED,
                    description: 'Unauthorized',
                    content: new OAT\JsonContent(ref: '#/components/schemas/ErrorResponse')
                ),
                new OAT\Response(
                    response: Response::HTTP_UNPROCESSABLE_ENTITY,
                    description: 'Validation error',
                    content: new OAT\JsonContent(ref: '#/components/schemas/ValidationErrorResponse')
                ),
            ],
            deprecated: false,
        )
    ]
    public function store(TravelRequest $request, TravelService $service): TravelResource|JsonResponse
    {
        try {
            return TravelResource::make(
                $service
                    ->fill($request->toDto(), Travel::make())
            );
        } catch (SomethingWentWrongException $e) {
            return response()
                ->json(
                    ErrorResource::make(['error' => $e->getMessage()])
                        ->toArray($request),
                    Response::HTTP_BAD_REQUEST
                );
        }
    }

    #[
        OAT\Put(
            path: '/travel/{travelId}',
            operationId: 'TravelUpdate',
            summary: 'Travel update',
            requestBody: new OAT\RequestBody(
                content: new OAT\JsonContent(
                    ref: '#/components/schemas/TravelRequest'
                )
            ),
            tags: ['Travel'],
            parameters: [
                new OAT\Parameter(ref: '#/components/parameters/Content-Type'),
                new OAT\Parameter(ref: '#/components/parameters/Accept'),
                new OAT\Parameter(ref: '#/components/parameters/Authorization'),
                new OAT\PathParameter(
                    parameter: 'travelId',
                    name: 'travelId',
                    description: 'Travel ID',
                    in: SwaggerInEnum::PATH,
                    required: true,
                    schema: new OAT\Schema(
                        description: 'Travel ID',
                        type: SwaggerTypeEnum::STRING,
                        format: SwaggerFormatEnum::UUID,
                        example: '4f4bd032-e7d4-402a-bdf6-aaf6be240d53',
                        nullable: false,
                    ),
                )
            ],
            responses: [
                new OAT\Response(
                    response: Response::HTTP_OK,
                    description: 'Travel info',
                    content: new OAT\JsonContent(
                        properties: [
                            new OAT\Property(
                                property: 'data',
                                type: SwaggerTypeEnum::OBJECT,
                                allOf: [
                                    new OAT\Schema(ref: '#/components/schemas/TravelResource')
                                ]
                            ),
                        ],
                    ),
                ),
                new OAT\Response(
                    response: Response::HTTP_BAD_REQUEST,
                    description: 'Something went wrong',
                    content: new OAT\JsonContent(ref: '#/components/schemas/ErrorResponse')
                ),
                new OAT\Response(
                    response: Response::HTTP_UNAUTHORIZED,
                    description: 'Unauthorized',
                    content: new OAT\JsonContent(ref: '#/components/schemas/ErrorResponse')
                ),
                new OAT\Response(
                    response: Response::HTTP_NOT_FOUND,
                    description: 'Travel not found',
                ),
                new OAT\Response(
                    response: Response::HTTP_UNPROCESSABLE_ENTITY,
                    description: 'Validation error',
                    content: new OAT\JsonContent(ref: '#/components/schemas/ValidationErrorResponse')
                ),
            ],
            deprecated: false,
        )
    ]
    public function update(Travel $travel, TravelRequest $request, TravelService $service): TravelResource|JsonResponse
    {
        try {
            return TravelResource::make(
                $service
                    ->fill($request->toDto(), $travel)
            );
        } catch (SomethingWentWrongException $e) {
            return response()
                ->json(
                    ErrorResource::make(['error' => $e->getMessage()])
                        ->toArray($request),
                    Response::HTTP_BAD_REQUEST
                );
        }
    }
}
