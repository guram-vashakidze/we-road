<?php

namespace App\Http\Resources;

use App\Helpers\Swagger\Enums\SwaggerTypeEnum;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;
use OpenApi\Attributes as OAT;

class SuccessResource extends JsonResource
{
    public static $wrap = null;

    #[
        OAT\Schema(
            schema: 'SuccessResponse',
            title: 'Success response',
            description: 'Success response',
            required: ['message', 'success', 'code'],
            properties: [
                new OAT\Property(
                    property: 'message',
                    description: 'Success message',
                    type: SwaggerTypeEnum::STRING,
                    example: 'Error message',
                    nullable: false
                ),
                new OAT\Property(
                    property: 'success',
                    description: 'Success flag',
                    type: SwaggerTypeEnum::BOOL,
                    example: true
                ),
                new OAT\Property(
                    property: 'code',
                    description: 'Response code',
                    type: SwaggerTypeEnum::INT,
                    example: Response::HTTP_OK
                ),
            ],
            type: SwaggerTypeEnum::OBJECT
        )
    ]
    public function toArray($request): array
    {
        return [
            'message' => $this->resource['message'],
            'success' => true,
            'code' => $this->resource['code'] ?? Response::HTTP_OK
        ];
    }
}
