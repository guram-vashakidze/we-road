<?php

namespace App\Models;

use App\Helpers\Model\HasUuids;
use Database\Factories\UserFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Carbon;
use Laravel\Passport\HasApiTokens;

/**
 * @property string $id
 * @property string $email
 * @property string $password
 * @property string $role_id
 * @property string $language_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @see self::language()
 * @property Language $language
 *
 * @see self::role()
 * @property Role $role
 *
 * @see self::scopeForEmail()
 * @method static Builder|self forEmail(string $email)
 *
 * @method static UserFactory factory($count = null, $state = [])
 */
class User extends Authenticatable
{
    use HasFactory;
    use HasUuids;
    use HasApiTokens;

    protected $fillable = [
        'email',
        'password',
        'role_id',
        'language_id',
    ];

    protected $hidden = [
        'password'
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s'
    ];

    public function language(): BelongsTo|Language
    {
        return $this->belongsTo(Language::class, 'language_id', 'id');
    }

    public function role(): BelongsTo|Role
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }

    public function scopeForEmail(Builder $builder, string $email): void
    {
        $builder->where('email', $email);
    }
}
