<?php

namespace Tests\Integration\Services;

use App\Dto\TourDto;
use App\Models\Tour;
use App\Models\Travel;
use App\Services\TourService;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class TourServiceTest extends TestCase
{
    use DatabaseTransactions;
    use WithFaker;

    private TourService $service;

    public function setUp(): void
    {
        parent::setUp();
        $this->service = resolve(TourService::class);
    }

    public function test_create_tour(): void
    {
        $travel = Travel::factory()->create();
        $tour = $this
            ->service
            ->fill(
                $dto = TourDto::init(
                    [
                        'name' => $this->faker->name,
                        'starting_date' => Carbon::now()->addDay()->toDateString(),
                        'ending_date' => Carbon::now()->addDays($travel->number_of_days + 1)->toDateString(),
                        'price' => 999.99
                    ]
                ),
                $travel,
                Tour::make()
            );
        $this->assertInstanceOf(Tour::class, $tour);
        $this
            ->assertDatabaseHas(
                Tour::class,
                [
                    'id' => $tour->id,
                    'name' => $dto->name,
                    'price' => 99999,
                    'starting_date' => $dto->startingDate->toDateString(),
                    'ending_date' => $dto->endingDate->toDateString()
                ]
            );
    }

    public function test_get_tour_list_default_sort(): void
    {
        $travel = Travel::factory()
            ->create();

        $tour1 = Tour::factory(['starting_date' => Carbon::now()->addDays(5)])
            ->forTravel($travel)
            ->create();

        $tour2 = Tour::factory(['starting_date' => Carbon::now()->addDays(2)])
            ->forTravel($travel)
            ->create();

        $tour3 = Tour::factory(['starting_date' => Carbon::now()->addDays(7)])
            ->forTravel($travel)
            ->create();

        Tour::factory()
            ->create();
        $result = $this
            ->service
            ->list($travel, [], 1, 5);
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertCount(3, $result->items());
        $this
            ->assertEquals(
                [
                    $tour2->id,
                    $tour1->id,
                    $tour3->id,
                ],
                collect($result->items())->pluck('id')->toArray()
            );
    }

    public function test_get_tour_list_sort_by_price(): void
    {
        $travel = Travel::factory()
            ->create();

        $tour1 = Tour::factory(['price' => 100])
            ->forTravel($travel)
            ->create();

        $tour2 = Tour::factory(['price' => 50])
            ->forTravel($travel)
            ->create();

        $tour3 = Tour::factory(['price' => 25])
            ->forTravel($travel)
            ->create();

        Tour::factory()
            ->create();

        $result = $this
            ->service
            ->list($travel, ['order' => ['column' => 'price']], 1, 5);
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertCount(3, $result->items());
        $this
            ->assertEquals(
                [
                    $tour3->id,
                    $tour2->id,
                    $tour1->id,
                ],
                collect($result->items())->pluck('id')->toArray()
            );
        $result = $this
            ->service
            ->list($travel, ['order' => ['column' => 'price', 'direction' => 'desc']], 1, 5);
        $this->assertCount(3, $result->items());
        $this
            ->assertEquals(
                [
                    $tour1->id,
                    $tour2->id,
                    $tour3->id,
                ],
                collect($result->items())->pluck('id')->toArray()
            );
    }

    public function test_get_tour_list_filter_by_price(): void
    {
        $travel = Travel::factory()
            ->create();

        $tour1 = Tour::factory(['price' => 100])
            ->forTravel($travel)
            ->create();

        $tour2 = Tour::factory(['price' => 50])
            ->forTravel($travel)
            ->create();

        $tour3 = Tour::factory(['price' => 25])
            ->forTravel($travel)
            ->create();

        Tour::factory()
            ->create();

        $result = $this
            ->service
            ->list($travel, ['price_from' => 30], 1, 5);
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertCount(2, $result->items());
        $this
            ->assertEquals(
                [
                    $tour1->id,
                    $tour2->id,
                ],
                collect($result->items())->pluck('id')->toArray()
            );

        $result = $this
            ->service
            ->list($travel, ['price_to' => 60], 1, 5);
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertCount(2, $result->items());
        $this
            ->assertEquals(
                [
                    $tour2->id,
                    $tour3->id,
                ],
                collect($result->items())->pluck('id')->toArray()
            );

        $result = $this
            ->service
            ->list($travel, ['price_from' => 30, 'price_to' => 60], 1, 5);
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertCount(1, $result->items());
        $this
            ->assertEquals(
                [
                    $tour2->id,
                ],
                collect($result->items())->pluck('id')->toArray()
            );
    }

    public function test_get_tour_list_filter_by_date(): void
    {
        $travel = Travel::factory()
            ->create();

        $tour1 = Tour::factory(['starting_date' => Carbon::now()->addDays(10)->toDateString()])
            ->forTravel($travel)
            ->create();

        $tour2 = Tour::factory(['starting_date' => Carbon::now()->addDays(5)->toDateString()])
            ->forTravel($travel)
            ->create();

        $tour3 = Tour::factory(['starting_date' => Carbon::now()->addDays(2)->toDateString()])
            ->forTravel($travel)
            ->create();

        Tour::factory()
            ->create();

        $result = $this
            ->service
            ->list($travel, ['date_from' => Carbon::now()->addDays(5)->toDateString()], 1, 5);
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertCount(2, $result->items());
        $this
            ->assertEquals(
                [
                    $tour2->id,
                    $tour1->id,
                ],
                collect($result->items())->pluck('id')->toArray()
            );

        $result = $this
            ->service
            ->list($travel, ['date_to' => Carbon::now()->addDays(5)->toDateString()], 1, 5);
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertCount(2, $result->items());
        $this
            ->assertEquals(
                [
                    $tour3->id,
                    $tour2->id,
                ],
                collect($result->items())->pluck('id')->toArray()
            );

        $result = $this
            ->service
            ->list(
                $travel,
                [
                    'date_from' => Carbon::now()->addDays(5)->toDateString(),
                    'date_to' => Carbon::now()->addDays(5)->toDateString()
                ],
                1,
                5
            );
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertCount(1, $result->items());
        $this
            ->assertEquals(
                [
                    $tour2->id,
                ],
                collect($result->items())->pluck('id')->toArray()
            );
    }
}
