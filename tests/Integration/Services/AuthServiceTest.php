<?php

namespace Tests\Integration\Services;

use App\Dto\AuthDto;
use App\Exceptions\UnauthorizedException;
use App\Models\User;
use App\Services\AuthService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class AuthServiceTest extends TestCase
{
    use DatabaseTransactions;
    use WithFaker;

    private AuthService $service;

    public function setUp(): void
    {
        parent::setUp();
        $this->service = resolve(AuthService::class);
    }

    public function test_login(): void
    {
        $factory = User::factory();
        $user = $factory->create();
        $token = $this
            ->service
            ->login(
                AuthDto::init(
                    [
                        'email' => $user->email,
                        'password' => $factory->getPassword()
                    ]
                )
            );
        $this->assertIsString($token);
        $this
            ->assertDatabaseHas(
                'oauth_access_tokens',
                [
                    'user_id' => $user->id,
                    'client_id' => DB::table('oauth_clients')
                        ->where('name', AuthService::PERSONAL_TOKEN_NAME)
                        ->value('id'),
                    'revoked' => 0
                ]
            );
    }

    public function test_login_incorrect_email(): void
    {
        $factory = User::factory();
        $factory->create();
        $this->expectException(UnauthorizedException::class);
        $this
            ->service
            ->login(
                AuthDto::init(
                    [
                        'email' => $this->faker->email,
                        'password' => $factory->getPassword()
                    ]
                )
            );
    }

    public function test_login_incorrect_password(): void
    {
        $factory = User::factory();
        $user = $factory->create();
        $this->expectException(UnauthorizedException::class);
        $this
            ->service
            ->login(
                AuthDto::init(
                    [
                        'email' => $user->email,
                        'password' => $this->faker->password
                    ]
                )
            );
    }

    public function test_logout(): void
    {
        $user = User::factory()->create();
        $token = $user->createToken(AuthService::PERSONAL_TOKEN_NAME);
        $user->withAccessToken($token->token);
        $this->service->logout($user->refresh());
        $this
            ->assertDatabaseHas(
                'oauth_access_tokens',
                [
                    'id' => $token->token->id,
                    'user_id' => $user->id,
                    'client_id' => DB::table('oauth_clients')
                        ->where('name', AuthService::PERSONAL_TOKEN_NAME)
                        ->value('id'),
                    'revoked' => 1
                ]
            );
    }
}
