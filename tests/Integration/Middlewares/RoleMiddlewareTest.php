<?php

namespace Tests\Integration\Middlewares;

use App\Enums\RoleEnum;
use App\Http\Middleware\RoleMiddleware;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class RoleMiddlewareTest extends TestCase
{
    use DatabaseTransactions;

    public function test_admin_access_success(): void
    {
        $user = User::factory()
            ->create();

        $request = new Request();
        $request
            ->setUserResolver(
                static fn ($guard = null) => $user
            );
        $response = (new RoleMiddleware())
            ->handle(
                $request,
                static fn (Request $request): Response => response()->json(),
                RoleEnum::ADMIN->value
            );

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function test_editor_access_success(): void
    {
        $user = User::factory()
            ->editor()
            ->create();

        $request = new Request();
        $request
            ->setUserResolver(
                static fn ($guard = null) => $user
            );
        $response = (new RoleMiddleware())
            ->handle(
                $request,
                static fn (Request $request): Response => response()->json(),
                RoleEnum::EDITOR->value
            );

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function test_mixed_access_success_1(): void
    {
        $user = User::factory()
            ->create();

        $request = new Request();
        $request
            ->setUserResolver(
                static fn ($guard = null) => $user
            );
        $response = (new RoleMiddleware())
            ->handle(
                $request,
                static fn (Request $request): Response => response()->json(),
                RoleEnum::ADMIN->value,
                RoleEnum::EDITOR->value
            );

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function test_mixed_access_success_2(): void
    {
        $user = User::factory()
            ->editor()
            ->create();

        $request = new Request();
        $request
            ->setUserResolver(
                static fn ($guard = null) => $user
            );
        $response = (new RoleMiddleware())
            ->handle(
                $request,
                static fn (Request $request): Response => response()->json(),
                RoleEnum::ADMIN->value,
                RoleEnum::EDITOR->value
            );

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function test_admin_access_fail(): void
    {
        $user = User::factory()
            ->create();

        $request = new Request();
        $request
            ->setUserResolver(
                static fn ($guard = null) => $user
            );
        $response = (new RoleMiddleware())
            ->handle(
                $request,
                static fn (Request $request): Response => response()->json(),
                RoleEnum::EDITOR->value
            );

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    public function test_editor_access_fail(): void
    {
        $user = User::factory()
            ->editor()
            ->create();

        $request = new Request();
        $request
            ->setUserResolver(
                static fn ($guard = null) => $user
            );
        $response = (new RoleMiddleware())
            ->handle(
                $request,
                static fn (Request $request): Response => response()->json(),
                RoleEnum::ADMIN->value
            );

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }
}
