<?php

namespace App\Http\Requests;

use App\Dto\TourDto;
use App\Helpers\Swagger\Enums\SwaggerFormatEnum;
use App\Helpers\Swagger\Enums\SwaggerTypeEnum;
use App\Rules\TourDatesRule;
use App\Rules\TourUniqRule;
use OpenApi\Attributes as OAT;

class TourRequest extends BaseFormRequest
{
    #[
        OAT\Schema(
            schema: "TourRequest",
            required: [
                'name',
                'starting_date',
                'ending_date',
                'price',
            ],
            properties: [
                new OAT\Property(
                    property: 'name',
                    description: 'Tour name',
                    type: SwaggerTypeEnum::STRING,
                    example: 'ITJOR20211125',
                    nullable: false
                ),
                new OAT\Property(
                    property: 'starting_date',
                    type: SwaggerTypeEnum::STRING,
                    format: SwaggerFormatEnum::DATE,
                    example: '2024-01-01',
                    nullable: false
                ),
                new OAT\Property(
                    property: 'ending_date',
                    type: SwaggerTypeEnum::STRING,
                    format: SwaggerFormatEnum::DATE,
                    example: '2024-01-10',
                    nullable: false
                ),
                new OAT\Property(
                    property: 'price',
                    type: SwaggerTypeEnum::NUMBER,
                    format: SwaggerFormatEnum::NUMERIC,
                    example: 100.00,
                    nullable: false
                ),
            ]
        )
    ]
    public function rules(): array
    {
        return [
            'name' => [
                'required',
                'string',
                new TourUniqRule()
            ],
            'starting_date' => [
                'required',
                'date_format:Y-m-d',
                'after_or_equal:today'
            ],
            'ending_date' => [
                'required',
                'date_format:Y-m-d',
                'after:starting_date',
                new TourDatesRule()
            ],
            'price' => [
                'required',
                'numeric',
                'gt:0'
            ],
        ];
    }

    public function toDto(): TourDto
    {
        return TourDto::init($this->validated());
    }
}
