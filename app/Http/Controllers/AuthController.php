<?php

namespace App\Http\Controllers;

use App\Exceptions\UnauthorizedException;
use App\Helpers\Swagger\Enums\SwaggerTypeEnum;
use App\Http\Requests\AuthRequest;
use App\Http\Resources\AuthResource;
use App\Http\Resources\ErrorResource;
use App\Http\Resources\SuccessResource;
use App\Services\AuthService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use OpenApi\Attributes as OAT;
use Symfony\Component\HttpFoundation\JsonResponse;

class AuthController extends Controller
{
    #[
        OAT\Post(
            path: '/auth/login',
            operationId: 'AuthLogin',
            summary: 'Auth login',
            requestBody: new OAT\RequestBody(
                content: new OAT\JsonContent(
                    ref: '#/components/schemas/AuthRequest'
                )
            ),
            tags: ['Auth'],
            parameters: [
                new OAT\Parameter(ref: '#/components/parameters/Content-Type'),
                new OAT\Parameter(ref: '#/components/parameters/Accept'),
            ],
            responses: [
                new OAT\Response(
                    response: Response::HTTP_OK,
                    description: 'Auth login token',
                    content: new OAT\JsonContent(
                        properties: [
                            new OAT\Property(
                                property: 'data',
                                type: SwaggerTypeEnum::OBJECT,
                                allOf: [
                                    new OAT\Schema(ref: '#/components/schemas/AuthResource')
                                ]
                            ),
                        ],
                    ),
                ),
                new OAT\Response(
                    response: Response::HTTP_UNAUTHORIZED,
                    description: 'Unauthorized',
                    content: new OAT\JsonContent(ref: '#/components/schemas/ErrorResponse')
                ),
                new OAT\Response(
                    response: Response::HTTP_UNPROCESSABLE_ENTITY,
                    description: 'Validation error',
                    content: new OAT\JsonContent(ref: '#/components/schemas/ValidationErrorResponse')
                ),
            ],
            deprecated: false,
        )
    ]
    public function login(AuthRequest $request, AuthService $service): JsonResponse|AuthResource
    {
        try {
            return AuthResource::make(['token' => $service->login($request->toDto())]);
        } catch (UnauthorizedException $e) {
            return response()
                ->json(
                    ErrorResource::make(
                        [
                            'error' => $e->getMessage(),
                            'code' => Response::HTTP_UNAUTHORIZED
                        ]
                    )
                    ->toArray($request),
                    Response::HTTP_UNAUTHORIZED
                );
        }
    }

    #[
        OAT\Post(
            path: '/auth/logout',
            operationId: 'AuthLogout',
            summary: 'Auth logout',
            tags: ['Auth'],
            parameters: [
                new OAT\Parameter(ref: '#/components/parameters/Content-Type'),
                new OAT\Parameter(ref: '#/components/parameters/Accept'),
                new OAT\Parameter(ref: '#/components/parameters/Authorization'),
            ],
            responses: [
                new OAT\Response(
                    response: Response::HTTP_OK,
                    description: 'Success logout',
                    content: new OAT\JsonContent(ref: '#/components/schemas/SuccessResponse'),
                ),
                new OAT\Response(
                    response: Response::HTTP_UNAUTHORIZED,
                    description: 'Unauthorized',
                ),
            ],
            deprecated: false,
        )
    ]
    public function logout(Request $request, AuthService $service): SuccessResource
    {
        $service->logout($request->user());
        return SuccessResource::make(['message' => trans('auth.logout')]);
    }
}
