#!/bin/bash

composer install --no-progress --prefer-dist --working-dir=/var/www/html

sleep 10

php artisan optimize:clear
php artisan key:generate
php artisan storage:link
php artisan migrate --no-interaction
php artisan db:seed

php artisan ide-helper:eloquent
php artisan ide-helper:meta
php artisan ide-helper:generate

php artisan key:generate --env=testing
php artisan migrate:fresh --no-interaction --env=testing
php artisan db:seed --env=testing

php artisan l5-swagger:generate

chown -R www-data:www-data storage
chown -R www-data:www-data bootstrap/cache

chmod -R 777 storage
chmod -R 777 bootstrap/cache

exec php-fpm --nodaemonize
