<?php

namespace App\Dto;

use Illuminate\Support\Carbon;

/**
 * @property-read string $name
 * @property-read float $price
 * @property-read Carbon $startingDate
 * @property-read Carbon $endingDate
 */
class TourDto extends BaseDto
{
    protected string $name;
    protected float $price;
    protected Carbon $startingDate;
    protected Carbon $endingDate;

    public static function init(array $args): self
    {
        $dto = new self();
        $dto->name = $args['name'];
        $dto->price = (float) $args['price'];
        $dto->startingDate = Carbon::parse($args['starting_date']);
        $dto->endingDate = Carbon::parse($args['ending_date']);
        return $dto;
    }
}
