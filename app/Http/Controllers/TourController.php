<?php

namespace App\Http\Controllers;

use App\Exceptions\SomethingWentWrongException;
use App\Helpers\Swagger\Enums\SwaggerFormatEnum;
use App\Helpers\Swagger\Enums\SwaggerInEnum;
use App\Helpers\Swagger\Enums\SwaggerTypeEnum;
use App\Http\Requests\TourIndexRequest;
use App\Http\Requests\TourRequest;
use App\Http\Resources\ErrorResource;
use App\Http\Resources\TourResource;
use App\Models\Tour;
use App\Models\Travel;
use App\Services\TourService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use OpenApi\Attributes as OAT;

class TourController extends Controller
{
    #[
        OAT\Get(
            path: '/travel/{travelSlug}/tour',
            operationId: 'TourIndex',
            summary: 'Tours list',
            tags: ['Tour'],
            parameters: [
                new OAT\Parameter(ref: '#/components/parameters/Content-Type'),
                new OAT\Parameter(ref: '#/components/parameters/Accept'),
                new OAT\PathParameter(
                    parameter: 'travelSlug',
                    name: 'travelSlug',
                    description: 'Travel ID',
                    in: SwaggerInEnum::PATH,
                    required: true,
                    schema: new OAT\Schema(
                        description: 'Travel ID',
                        type: SwaggerTypeEnum::STRING,
                        format: SwaggerFormatEnum::UUID,
                        example: '4f4bd032-e7d4-402a-bdf6-aaf6be240d53',
                        nullable: false,
                    ),
                ),
                new OAT\QueryParameter(
                    parameter: 'date_from',
                    name: 'date_from',
                    description: 'Filter by `starting_date`',
                    in: SwaggerInEnum::QUERY,
                    required: false,
                    schema: new OAT\Schema(
                        type: SwaggerTypeEnum::STRING,
                        format: SwaggerFormatEnum::DATE,
                        example: '2024-01-01',
                        nullable: false
                    )
                ),
                new OAT\QueryParameter(
                    parameter: 'date_to',
                    name: 'date_to',
                    description: 'Filter by `starting_date`. Must be grater or equals `date_from`',
                    in: SwaggerInEnum::QUERY,
                    required: false,
                    schema: new OAT\Schema(
                        type: SwaggerTypeEnum::STRING,
                        format: SwaggerFormatEnum::DATE,
                        example: '2024-01-10',
                        nullable: false
                    )
                ),
                new OAT\QueryParameter(
                    parameter: 'price_from',
                    name: 'price_from',
                    description: 'Filter by `price`',
                    in: SwaggerInEnum::QUERY,
                    required: false,
                    schema: new OAT\Schema(
                        type: SwaggerTypeEnum::NUMBER,
                        format: SwaggerFormatEnum::NUMERIC,
                        example: 100.00,
                        nullable: false
                    )
                ),
                new OAT\QueryParameter(
                    parameter: 'price_to',
                    name: 'price_from',
                    description: 'Filter by `price`. Must be grater or equals `price_from`',
                    in: SwaggerInEnum::QUERY,
                    required: false,
                    schema: new OAT\Schema(
                        type: SwaggerTypeEnum::NUMBER,
                        format: SwaggerFormatEnum::NUMERIC,
                        example: 200.00,
                        nullable: false
                    )
                ),
                new OAT\QueryParameter(
                    parameter: 'order',
                    name: 'order',
                    description: 'Ordering setting. Default ordering by `starting_date`/`asc`',
                    in: SwaggerInEnum::QUERY,
                    required: false,
                    schema: new OAT\Schema(
                        properties: [
                            new OAT\Property(
                                property: 'column',
                                description: 'Sorting column. Required if `order` is present in request',
                                type: SwaggerTypeEnum::STRING,
                                enum: ['price'],
                                example: 'price',
                                nullable: false
                            ),
                            new OAT\Property(
                                property: 'direction',
                                description: 'Sorting direction',
                                type: SwaggerTypeEnum::STRING,
                                default: 'asc',
                                enum: ['asc', 'desc'],
                                example: 'asc',
                                nullable: false
                            ),
                        ],
                        type: SwaggerTypeEnum::OBJECT,
                        nullable: false
                    )
                ),
                new OAT\QueryParameter(
                    parameter: 'page',
                    name: 'page',
                    description: 'Pagination parameter',
                    in: SwaggerInEnum::QUERY,
                    required: false,
                    schema: new OAT\Schema(
                        type: SwaggerTypeEnum::INT,
                        default: 1,
                        minimum: 1,
                        example: 2,
                        nullable: false
                    )
                ),
                new OAT\QueryParameter(
                    parameter: 'per_page',
                    name: 'per_page',
                    description: 'Pagination parameter',
                    in: SwaggerInEnum::QUERY,
                    required: false,
                    schema: new OAT\Schema(
                        type: SwaggerTypeEnum::INT,
                        default: 15,
                        minimum: 1,
                        example: 2,
                        nullable: false
                    )
                ),
            ],
            responses: [
                new OAT\Response(
                    response: Response::HTTP_OK,
                    description: 'Tours list',
                    content: new OAT\JsonContent(
                        properties: [
                            new OAT\Property(
                                property: 'data',
                                type: SwaggerTypeEnum::ARRAY,
                                items: new OAT\Items(
                                    ref: '#/components/schemas/TourResource',
                                    type: SwaggerTypeEnum::OBJECT
                                ),
                            ),
                            new OAT\Property(
                                property: 'links',
                                properties: [
                                    new OAT\Property(
                                        property: 'first',
                                        type: SwaggerTypeEnum::STRING,
                                        format: SwaggerFormatEnum::URI,
                                        example: 'http://we-road.loc/travel/qui-tempore-corporis-commodi-earum-sit-laboriosam/tour?page=2',
                                        nullable: false,
                                    ),
                                    new OAT\Property(
                                        property: 'last',
                                        type: SwaggerTypeEnum::STRING,
                                        format: SwaggerFormatEnum::URI,
                                        example: 'http://we-road.loc/travel/qui-tempore-corporis-commodi-earum-sit-laboriosam/tour?page=10',
                                        nullable: false,
                                    ),
                                    new OAT\Property(
                                        property: 'prev',
                                        type: SwaggerTypeEnum::STRING,
                                        format: SwaggerFormatEnum::URI,
                                        example: 'http://we-road.loc/travel/qui-tempore-corporis-commodi-earum-sit-laboriosam/tour?page=1',
                                        nullable: true,
                                    ),
                                    new OAT\Property(
                                        property: 'next',
                                        type: SwaggerTypeEnum::STRING,
                                        format: SwaggerFormatEnum::URI,
                                        example: 'http://we-road.loc/travel/qui-tempore-corporis-commodi-earum-sit-laboriosam/tour?page=3',
                                        nullable: true,
                                    ),
                                ],
                                type: SwaggerTypeEnum::OBJECT
                            ),
                            new OAT\Property(
                                property: 'meta',
                                properties: [
                                    new OAT\Property(
                                        property: 'current_page',
                                        type: SwaggerTypeEnum::INT,
                                        minimum: 1,
                                        example: 1,
                                        nullable: false,
                                    ),
                                    new OAT\Property(
                                        property: 'from',
                                        type: SwaggerTypeEnum::INT,
                                        minimum: 1,
                                        example: 1,
                                        nullable: false,
                                    ),
                                    new OAT\Property(
                                        property: 'last_page',
                                        type: SwaggerTypeEnum::INT,
                                        minimum: 1,
                                        example: 1,
                                        nullable: false,
                                    ),
                                    new OAT\Property(
                                        property: 'links',
                                        type: SwaggerTypeEnum::ARRAY,
                                        items: new OAT\Items(
                                            properties: [
                                                new OAT\Property(
                                                    property: 'url',
                                                    type: SwaggerTypeEnum::STRING,
                                                    format: SwaggerFormatEnum::URI,
                                                    example: 'http://we-road.loc/travel/qui-tempore-corporis-commodi-earum-sit-laboriosam/tour?page=1',
                                                    nullable: true,
                                                ),
                                                new OAT\Property(
                                                    property: 'label',
                                                    type: SwaggerTypeEnum::STRING,
                                                    nullable: false,
                                                ),
                                                new OAT\Property(
                                                    property: 'active',
                                                    type: SwaggerTypeEnum::BOOL,
                                                    example: true,
                                                    nullable: false,
                                                ),
                                            ],
                                            type: SwaggerTypeEnum::OBJECT
                                        ),
                                    ),
                                    new OAT\Property(
                                        property: 'path',
                                        type: SwaggerTypeEnum::STRING,
                                        format: SwaggerFormatEnum::URI,
                                        example: 'http://we-road.loc/travel/qui-tempore-corporis-commodi-earum-sit-laboriosam/tour',
                                        nullable: false,
                                    ),
                                    new OAT\Property(
                                        property: 'per_page',
                                        type: SwaggerTypeEnum::INT,
                                        minimum: 1,
                                        example: 15,
                                        nullable: false,
                                    ),
                                    new OAT\Property(
                                        property: 'to',
                                        type: SwaggerTypeEnum::INT,
                                        minimum: 1,
                                        example: 10,
                                        nullable: false,
                                    ),
                                    new OAT\Property(
                                        property: 'total',
                                        type: SwaggerTypeEnum::INT,
                                        minimum: 1,
                                        example: 10,
                                        nullable: false,
                                    ),
                                ],
                                type: SwaggerTypeEnum::OBJECT,
                            ),
                        ],
                    ),
                ),
                new OAT\Response(
                    response: Response::HTTP_NOT_FOUND,
                    description: 'Travel not found',
                ),
            ],
            deprecated: false,
        )
    ]
    public function index(TourIndexRequest $request, Travel $travel, TourService $service): AnonymousResourceCollection
    {
        return TourResource::collection(
            $service
                ->list(
                    $travel,
                    $request->getFilter(),
                    $request->getPage(),
                    $request->getPerPage()
                )
        );
    }

    #[
        OAT\Get(
            path: '/travel/{travelId}/tour/{tourId}',
            operationId: 'TourShow',
            summary: 'Tour show',
            tags: ['Tour'],
            parameters: [
                new OAT\Parameter(ref: '#/components/parameters/Content-Type'),
                new OAT\Parameter(ref: '#/components/parameters/Accept'),
                new OAT\Parameter(ref: '#/components/parameters/Authorization'),
                new OAT\PathParameter(
                    parameter: 'travelId',
                    name: 'travelId',
                    description: 'Travel ID',
                    in: SwaggerInEnum::PATH,
                    required: true,
                    schema: new OAT\Schema(
                        description: 'Travel ID',
                        type: SwaggerTypeEnum::STRING,
                        format: SwaggerFormatEnum::UUID,
                        example: '4f4bd032-e7d4-402a-bdf6-aaf6be240d53',
                        nullable: false,
                    ),
                ),
                new OAT\PathParameter(
                    parameter: 'tourId',
                    name: 'tourId',
                    description: 'Tour ID',
                    in: SwaggerInEnum::PATH,
                    required: true,
                    schema: new OAT\Schema(
                        description: 'Tour ID',
                        type: SwaggerTypeEnum::STRING,
                        format: SwaggerFormatEnum::UUID,
                        example: '4f4bd032-e7d4-402a-bdf6-aaf6be240d53',
                        nullable: false,
                    ),
                ),
            ],
            responses: [
                new OAT\Response(
                    response: Response::HTTP_OK,
                    description: 'Tour info',
                    content: new OAT\JsonContent(
                        properties: [
                            new OAT\Property(
                                property: 'data',
                                type: SwaggerTypeEnum::OBJECT,
                                allOf: [
                                    new OAT\Schema(ref: '#/components/schemas/TourResource')
                                ]
                            ),
                        ],
                    ),
                ),
                new OAT\Response(
                    response: Response::HTTP_UNAUTHORIZED,
                    description: 'Unauthorized',
                    content: new OAT\JsonContent(ref: '#/components/schemas/ErrorResponse')
                ),
                new OAT\Response(
                    response: Response::HTTP_NOT_FOUND,
                    description: 'Travel/tour not found',
                ),
            ],
            deprecated: false,
        )
    ]
    public function show(Travel $travel, Tour $tour): TourResource|JsonResponse
    {
        if ($tour->travel_id !== $travel->id) {
            return response()
                ->json(
                    ErrorResource::make(['error' => 'Not found', 'code' => Response::HTTP_NOT_FOUND])
                        ->toArray(request()),
                    Response::HTTP_NOT_FOUND
                );
        }
        return TourResource::make($tour);
    }

    #[
        OAT\Post(
            path: '/travel/{travelId}/tour',
            operationId: 'TourStore',
            summary: 'Tour store',
            requestBody: new OAT\RequestBody(
                content: new OAT\JsonContent(
                    ref: '#/components/schemas/TourRequest'
                )
            ),
            tags: ['Tour'],
            parameters: [
                new OAT\Parameter(ref: '#/components/parameters/Content-Type'),
                new OAT\Parameter(ref: '#/components/parameters/Accept'),
                new OAT\Parameter(ref: '#/components/parameters/Authorization'),
                new OAT\PathParameter(
                    parameter: 'travelId',
                    name: 'travelId',
                    description: 'Travel ID',
                    in: SwaggerInEnum::PATH,
                    required: true,
                    schema: new OAT\Schema(
                        description: 'Travel ID',
                        type: SwaggerTypeEnum::STRING,
                        format: SwaggerFormatEnum::UUID,
                        example: '4f4bd032-e7d4-402a-bdf6-aaf6be240d53',
                        nullable: false,
                    ),
                ),
            ],
            responses: [
                new OAT\Response(
                    response: Response::HTTP_CREATED,
                    description: 'Tour info',
                    content: new OAT\JsonContent(
                        properties: [
                            new OAT\Property(
                                property: 'data',
                                type: SwaggerTypeEnum::OBJECT,
                                allOf: [
                                    new OAT\Schema(ref: '#/components/schemas/TourResource')
                                ]
                            ),
                        ],
                    ),
                ),
                new OAT\Response(
                    response: Response::HTTP_BAD_REQUEST,
                    description: 'Something went wrong',
                    content: new OAT\JsonContent(ref: '#/components/schemas/ErrorResponse')
                ),
                new OAT\Response(
                    response: Response::HTTP_UNAUTHORIZED,
                    description: 'Unauthorized',
                    content: new OAT\JsonContent(ref: '#/components/schemas/ErrorResponse')
                ),
                new OAT\Response(
                    response: Response::HTTP_UNPROCESSABLE_ENTITY,
                    description: 'Validation error',
                    content: new OAT\JsonContent(ref: '#/components/schemas/ValidationErrorResponse')
                ),
            ],
            deprecated: false,
        )
    ]
    public function store(TourRequest $request, Travel $travel, TourService $service): TourResource|JsonResponse
    {
        try {
            return TourResource::make(
                $service
                    ->fill($request->toDto(), $travel, Tour::make())
            );
        } catch (SomethingWentWrongException $e) {
            return response()
                ->json(
                    ErrorResource::make(['error' => $e->getMessage()])
                        ->toArray($request),
                    Response::HTTP_BAD_REQUEST
                );
        }
    }
}
