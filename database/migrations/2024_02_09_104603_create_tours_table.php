<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create(
            'tours',
            static function (Blueprint $table): void {
                $table
                    ->uuid('id')
                    ->primary();
                $table
                    ->foreignUuid('travel_id')
                    ->references('id')
                    ->on('travels')
                    ->cascadeOnDelete()
                    ->cascadeOnUpdate();
                $table
                    ->string('name');
                $table
                    ->date('starting_date');
                $table
                    ->date('ending_date');
                $table
                    ->unsignedInteger('price');
                $table
                    ->timestamps();
                $table
                    ->unique(['travel_id', 'name']);
            }
        );
    }

    public function down(): void
    {
        Schema::dropIfExists('tours');
    }
};
