<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class TravelSeeder extends Seeder
{
    public function run(): void
    {
        $travels = json_decode(
            file_get_contents(storage_path('samples/travels.json')),
            true
        );
        foreach ($travels as $travel) {
            DB::table('travels')
                ->updateOrInsert(
                    [
                        'id' => $travel['id']
                    ],
                    [
                        'slug' => $travel['slug'],
                        'name' => $travel['name'],
                        'description' =>  $travel['description'],
                        'number_of_days' => $travel['numberOfDays'],
                        'mood_nature' => $travel['moods']['nature'],
                        'mood_relax' => $travel['moods']['relax'],
                        'mood_history' => $travel['moods']['history'],
                        'mood_culture' => $travel['moods']['culture'],
                        'mood_party' => $travel['moods']['party'],
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString(),
                    ]
                );
        }
    }
}
