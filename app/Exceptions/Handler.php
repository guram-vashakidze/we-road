<?php

namespace App\Exceptions;

use App\Http\Resources\ErrorResource;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    protected $levels = [];

    protected $dontReport = [];

    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    public function register()
    {
        $this->reportable(function (Throwable $e) {
        });
    }

    public function render($request, Throwable $e)
    {
        if ($e instanceof HttpException) {
            return match ($e->getStatusCode()) {
                Response::HTTP_UNAUTHORIZED => response()
                    ->json(
                        ErrorResource::make(
                            [
                                'error' => 'Unauthorized',
                                'code' => Response::HTTP_UNAUTHORIZED
                            ]
                        )
                            ->toArray($request),
                        Response::HTTP_UNAUTHORIZED
                    ),
                Response::HTTP_NOT_FOUND => response()
                    ->json(
                        ErrorResource::make(
                            [
                                'error' => 'Not found',
                                'code' => Response::HTTP_NOT_FOUND
                            ]
                        )
                            ->toArray($request),
                        Response::HTTP_NOT_FOUND
                    ),
                Response::HTTP_UNPROCESSABLE_ENTITY => dd($e) && response()
                    ->json(
                        ErrorResource::make(
                            [
                                'error' => 'Not found',
                                'code' => Response::HTTP_NOT_FOUND
                            ]
                        )
                            ->toArray($request),
                        Response::HTTP_NOT_FOUND
                    ),
                default => parent::render($request, $e)
            };
        }
        return parent::render($request, $e);
    }
}
