<?php

namespace App\Http\Resources;

use App\Helpers\Swagger\Enums\SwaggerTypeEnum;
use Illuminate\Http\Resources\Json\JsonResource;
use OpenApi\Attributes as OAT;

class AuthResource extends JsonResource
{
    #[
        OAT\Schema(
            schema: "AuthResource",
            required: ['token'],
            properties: [
                new OAT\Property(
                    property: 'token',
                    description: 'Auth token',
                    type: SwaggerTypeEnum::STRING,
                    example: '',
                    nullable: false
                ),
            ]
        )
    ]
    public function toArray($request): array
    {
        return [
            'token' => $this->resource['token']
        ];
    }
}
