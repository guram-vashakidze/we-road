<?php

namespace App\Dto;

use Exception;
use Illuminate\Support\Collection;
use ReflectionClass;
use ReflectionProperty;

abstract class BaseDto
{
    abstract public static function init(array $args): self;

    public function __get(string $name)
    {
        if (property_exists($this, $name)) {
            return $this->{$name};
        }

        return null;
    }

    public function __call($name, $arguments)
    {
        $result = $this->checkIssetMethod($name);

        if ($result === null) {
            throw new Exception("Method " . $name . " not found", $this, $name, $arguments);
        }

        return $result;
    }

    protected function checkIssetMethod(string $name): ?bool
    {
        if (!preg_match("/^isset/", $name)) {
            return null;
        }

        $propertyName = lcfirst(
            preg_replace("/^isset/", "", $name)
        );

        return $this->issetProperty($propertyName);
    }

    protected function issetProperty(string $propertyName): bool
    {
        if (!property_exists($this, $propertyName)) {
            return false;
        }

        if (!isset($this->{$propertyName}) || $this->{$propertyName} === null) {
            return false;
        }

        return true;
    }

    public function toArray(bool $withNull = true): ?array
    {
        $reflection = new ReflectionClass($this);
        $properties = $reflection->getProperties(ReflectionProperty::IS_PROTECTED);

        foreach ($properties as $property) {
            if (!$this->issetProperty($property->name)) {
                if (!$withNull) {
                    continue;
                }

                $result[$property->name] = null;
                continue;
            }
            $value = $this->renderToArrayValue($this->{$property->name}, $withNull);

            if ($value === null && !$withNull) {
                continue;
            }

            $result[$property->name] = $value;
        }

        return !empty($result) ? $result : null;
    }

    private function renderToArrayValue($value, bool $withNull)
    {
        if ($value instanceof BaseDto) {
            return $value->toArray($withNull);
        } elseif ($value instanceof Collection) {
            return $value->toArray();
        } elseif (is_array($value)) {
            foreach ($value as $key => $item) {
                $item = $this->renderToArrayValue($item, $withNull);
                if ($item === null && !$withNull) {
                    continue;
                }
                $result[$key] = $item;
            }
            return !empty($result) ? $result : null;
        } else {
            return $value;
        }
    }
}
