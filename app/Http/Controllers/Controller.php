<?php

namespace App\Http\Controllers;

use App\Helpers\Swagger\Enums\SwaggerInEnum;
use App\Helpers\Swagger\Enums\SwaggerTypeEnum;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use OpenApi\Attributes as OAT;

#[
    OAT\Info(
        version: "1.0.0",
        description: "WeRoad Test",
        title: "WeRoad",
        contact: new OAT\Contact(
            email: 'uranik156@gmail.com'
        )
    ),
    OAT\Server(
        url: '/',
        description: 'Current server'
    ),
    OAT\HeaderParameter(
        parameter: 'Authorization',
        name: 'Authorization',
        in: SwaggerInEnum::HEADER,
        required: true,
        schema: new OAT\Schema(
            type: SwaggerTypeEnum::STRING,
            example: "Bearer <authorization_token>"
        )
    ),
    OAT\HeaderParameter(
        parameter: 'Accept',
        name: 'Accept',
        in: SwaggerInEnum::HEADER,
        required: true,
        schema: new OAT\Schema(
            type: SwaggerTypeEnum::STRING,
            example: "application/json"
        )
    ),
    OAT\HeaderParameter(
        parameter: 'Content-Type',
        name: 'Content-Type',
        in: SwaggerInEnum::HEADER,
        required: true,
        schema: new OAT\Schema(
            type: SwaggerTypeEnum::STRING,
            example: "application/json"
        )
    )
]

class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;
}
