<!DOCTYPE html>
<html lang="en">
<head>
    <title>API Docs</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
<redoc spec-url="/redoc/api.json"></redoc>
<script src="https://cdn.redoc.ly/redoc/latest/bundles/redoc.standalone.js"> </script>
</body>

</html>
