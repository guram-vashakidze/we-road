<?php

namespace Database\Seeders;

use App\Enums\RoleEnum;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RoleSeeder extends Seeder
{
    public function run(): void
    {
        foreach (RoleEnum::cases() as $role) {
            DB::table('roles')
                ->insertOrIgnore(
                    [
                        'id' => Str::orderedUuid()->toString(),
                        'name' => $role->value
                    ]
                );
        }
    }
}
