<?php

namespace App\Http\Middleware;

use App\Http\Resources\ErrorResource;
use App\Models\Travel;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

readonly class PublicTravelMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Closure(Request): (Response) $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        /**@var Travel $travel*/
        $travel = $request
            ->route()
            ?->parameter('travel');
        if ($travel->public) {
            return $next($request);
        }
        if ($request->user()) {
            return $next($request);
        }
        return response()
            ->json(
                ErrorResource::make(
                    [
                        'error' => 'Not found',
                        'code' => Response::HTTP_NOT_FOUND
                    ]
                )
                    ->toArray($request),
                Response::HTTP_NOT_FOUND
            );
    }
}
