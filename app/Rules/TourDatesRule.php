<?php

namespace App\Rules;

use App\Models\Travel;
use Closure;
use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Support\Carbon;
use Illuminate\Translation\PotentiallyTranslatedString;

readonly class TourDatesRule implements ValidationRule, DataAwareRule
{
    private Carbon $startingDate;

    /**
     * @param Closure(string): PotentiallyTranslatedString $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        /**@var Travel $travel*/
        $travel = request()
            ->route()
            ?->parameter('travel');
        if (!$travel) {
            return;
        }
        if (Carbon::parse($value)->diffInDays($this->startingDate) === $travel->number_of_days) {
            return;
        }
        $fail(trans('error.tour_dates_interval', ['travel' => $travel->name, 'days' => $travel->number_of_days]));
    }

    public function setData(array $data): void
    {
        $this->startingDate = Carbon::parse($data['starting_date']);
    }
}
