<?php

namespace Tests\Feature\Controllers;

use App\Models\Tour;
use App\Models\Travel;
use App\Services\TourService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Carbon;
use Tests\Contracts\WithoutAuthorization;
use Tests\Helpers\AuthorizationHelper;
use Tests\TestCase;

class TourControllerTest extends TestCase implements WithoutAuthorization
{
    use DatabaseTransactions;
    use AuthorizationHelper;
    use WithFaker;

    public function test_tour_list(): void
    {
        $travel = Travel::factory()
            ->has(
                Tour::factory()
                    ->count(5),
                'tours'
            )
            ->create();
        $this
            ->getJson(route('travel.tour.index', $travel->slug))
            ->assertOk()
            ->assertJsonCount(5, 'data')
            ->assertJson(
                [
                    'data' => [
                        [
                            'id' => $travel->tours[0]->id
                        ],
                        [
                            'id' => $travel->tours[1]->id
                        ],
                        [
                            'id' => $travel->tours[2]->id
                        ],
                        [
                            'id' => $travel->tours[3]->id
                        ],
                        [
                            'id' => $travel->tours[4]->id
                        ],
                    ]
                ]
            );
    }

    public function test_tour_list_for_not_public_travel(): void
    {
        $travel = Travel::factory(['public' => false])
            ->has(
                Tour::factory()
                    ->count(5),
                'tours'
            )
            ->create();
        $this
            ->getJson(route('travel.tour.index', $travel->slug))
            ->assertNotFound();
        $this->authorize();
        $this
            ->getJson(route('travel.tour.index', $travel->slug))
            ->assertOk()
            ->assertJsonCount(5, 'data')
            ->assertJson(
                [
                    'data' => [
                        [
                            'id' => $travel->tours[0]->id
                        ],
                        [
                            'id' => $travel->tours[1]->id
                        ],
                        [
                            'id' => $travel->tours[2]->id
                        ],
                        [
                            'id' => $travel->tours[3]->id
                        ],
                        [
                            'id' => $travel->tours[4]->id
                        ],
                    ]
                ]
            );
    }

    public function test_show_tour(): void
    {
        $this->authorize();
        $tour = Tour::factory()->create();
        $this
            ->getJson(route('travel.tour.show', ['travel' => $tour->travel_id, 'tour' => $tour->id]))
            ->assertOk()
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'starting_date',
                        'ending_date',
                        'price',
                        'created_at',
                        'updated_at',
                    ]
                ]
            )
            ->assertJson(
                [
                    'data' => [
                        'id' => $tour->id,
                        'name' => $tour->name,
                        'starting_date' => $tour->starting_date->toDateString(),
                        'ending_date' => $tour->ending_date->toDateString(),
                        'price' => $tour->price,
                        'created_at' => $tour->created_at->toDateTimeString(),
                        'updated_at' => $tour->updated_at->toDateTimeString(),
                    ]
                ]
            );
    }

    public function test_show_tour_failed(): void
    {
        $this->authorize();
        $tour = Tour::factory()->create();
        $this
            ->getJson(route('travel.tour.show', ['travel' => Travel::factory()->create()->id, 'tour' => $tour->id]))
            ->assertNotFound();
    }

    public function test_create_tour(): void
    {
        $this->authorize();
        $spy = $this
            ->spy(TourService::class)
            ->makePartial();
        $travel = Travel::factory()->create();
        $this
            ->postJson(
                route('travel.tour.store', $travel),
                $request = [
                    'name' => $this->faker->name,
                    'starting_date' => Carbon::now()->toDateString(),
                    'ending_date' => Carbon::now()->addDays($travel->number_of_days)->toDateString(),
                    'price' => 888.88,
                ]
            )
            ->assertCreated()
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'starting_date',
                        'ending_date',
                        'price',
                        'created_at',
                        'updated_at',
                    ]
                ]
            )
            ->assertJson(
                [
                    'data' => [
                        'name' => $request['name'],
                        'starting_date' => $request['starting_date'],
                        'ending_date' => $request['ending_date'],
                        'price' => $request['price'],
                    ]
                ]
            );
        $this
            ->assertDatabaseHas(
                Tour::class,
                [
                    'name' => $request['name'],
                    'starting_date' => $request['starting_date'],
                    'ending_date' => $request['ending_date'],
                    'price' => 88888,
                ]
            );
        $spy->shouldHaveReceived('fill');
    }

    public function test_create_tour_by_editor(): void
    {
        $this->authorize();
        $this->changeToEditor();
        $spy = $this
            ->spy(TourService::class)
            ->makePartial();
        $travel = Travel::factory()->create();
        $this
            ->postJson(
                route('travel.tour.store', $travel),
                $request = [
                    'name' => $this->faker->name,
                    'starting_date' => Carbon::now()->toDateString(),
                    'ending_date' => Carbon::now()->addDays($travel->number_of_days)->toDateString(),
                    'price' => 888.88,
                ]
            )
            ->assertUnauthorized();
        $this
            ->assertDatabaseMissing(
                Tour::class,
                [
                    'name' => $request['name'],
                    'starting_date' => $request['starting_date'],
                    'ending_date' => $request['ending_date'],
                    'price' => 88888,
                ]
            );
        $spy->shouldNotHaveReceived('fill');
    }
}
