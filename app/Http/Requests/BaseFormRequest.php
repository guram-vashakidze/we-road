<?php

namespace App\Http\Requests;

use App\Helpers\Swagger\Enums\SwaggerTypeEnum;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use OpenApi\Attributes as OAT;

abstract class BaseFormRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    #[
        OAT\Schema(
            schema: 'ValidationErrorResponse',
            title: 'Validation error response',
            description: 'Validation error response',
            properties: [
                new OAT\Property(
                    property: 'error',
                    description: 'Error message',
                    type: SwaggerTypeEnum::STRING,
                    example: 'Error message',
                    nullable: false
                ),
                new OAT\Property(
                    property: 'success',
                    description: 'Success flag',
                    type: SwaggerTypeEnum::BOOL,
                    example: false
                ),
                new OAT\Property(
                    property: 'code',
                    description: 'Response code',
                    type: SwaggerTypeEnum::INT,
                    example: Response::HTTP_UNPROCESSABLE_ENTITY
                ),
                new OAT\Property(
                    property: 'errors',
                    description: 'Object with fields name and fields errors',
                    type: SwaggerTypeEnum::OBJECT,
                    example: [
                        'field_1' => [
                            'error_1_1',
                            'error_1_2',
                        ],
                        'field_2' => [
                            'error_2_1',
                            'error_2_2',
                        ],
                    ],
                    allOf: [
                        new OAT\Schema(
                            description: 'Additional field name',
                            type: SwaggerTypeEnum::ARRAY,
                            items: new OAT\Items(
                                type: SwaggerTypeEnum::STRING
                            ),
                            additionalProperties: true,
                        )
                    ]
                )
            ]
        )
    ]
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'error' => 'Request validation error',
            'errors' => $validator->errors(),
            'success' => false,
            'code' => Response::HTTP_UNPROCESSABLE_ENTITY
        ], Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    abstract public function rules(): array;
}
