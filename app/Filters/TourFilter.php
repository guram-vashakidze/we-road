<?php

namespace App\Filters;

use App\Casts\MoneyCast;
use App\Models\Tour;
use EloquentFilter\ModelFilter;

/**
 * @mixin Tour
 */
class TourFilter extends ModelFilter
{
    public function priceFrom(float $price): void
    {
        $this
            ->where(
                'price',
                '>=',
                (new MoneyCast())->set(Tour::make(), 'price', $price, [])
            );
    }

    public function priceTo(float $price): void
    {
        $this
            ->where(
                'price',
                '<=',
                (new MoneyCast())->set(Tour::make(), 'price', $price, [])
            );
    }

    public function dateFrom(string $date): void
    {
        $this
            ->where('starting_date', '>=', $date);
    }

    public function dateTo(string $date): void
    {
        $this
            ->where('starting_date', '<=', $date);
    }

    public function order(array $order): void
    {
        $this
            ->withoutGlobalScope('default_order')
            ->orderBy($order['column'], $order['direction'] ?? 'asc')
            ->orderBy('id');
    }
}
