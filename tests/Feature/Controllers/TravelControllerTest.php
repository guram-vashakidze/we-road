<?php

namespace Tests\Feature\Controllers;

use App\Models\Travel;
use App\Services\TravelService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\Helpers\AuthorizationHelper;
use Tests\TestCase;

class TravelControllerTest extends TestCase
{
    use DatabaseTransactions;
    use AuthorizationHelper;
    use WithFaker;

    public function test_show_travel(): void
    {
        $travel = Travel::factory()
            ->create();
        $this
            ->getJson(route('travel.show', $travel))
            ->assertOk()
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'slug',
                        'description',
                        'number_of_days',
                        'number_of_nights',
                        'public',
                        'moods',
                        'created_at',
                        'updated_at',
                    ]
                ]
            )
            ->assertJson(
                [
                    'data' => [
                        'id' => $travel->id,
                        'name' => $travel->name,
                        'slug' => $travel->slug,
                        'description' => $travel->description,
                        'number_of_days' => $travel->number_of_days,
                        'number_of_nights' => $travel->number_of_days - 1,
                        'public' => $travel->public,
                        'moods' => $travel->moods,
                        'created_at' => $travel->created_at->toDateTimeString(),
                        'updated_at' => $travel->updated_at->toDateTimeString(),
                    ]
                ]
            );
    }

    public function test_create_travel(): void
    {
        $spy = $this
            ->spy(TravelService::class)
            ->makePartial();
        $this
            ->postJson(
                route('travel.store'),
                $request = [
                    'name' => $this->faker->name,
                    'description' => $this->faker->sentence,
                    'number_of_days' => mt_rand(2, 12),
                    'public' => true,
                    'moods' => [
                        'nature' => mt_rand(0, 100),
                        'relax' => mt_rand(0, 100),
                        'history' => mt_rand(0, 100),
                        'culture' => mt_rand(0, 100),
                        'party' => mt_rand(0, 100),
                    ]
                ]
            )
            ->assertCreated()
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'slug',
                        'description',
                        'number_of_days',
                        'number_of_nights',
                        'public',
                        'moods',
                        'created_at',
                        'updated_at',
                    ]
                ]
            )
            ->assertJson(
                [
                    'data' => [
                        'name' => $request['name'],
                        'slug' => Str::slug($request['name']),
                        'description' => $request['description'],
                        'number_of_days' => $request['number_of_days'],
                        'number_of_nights' => $request['number_of_days'] - 1,
                        'public' => $request['public'],
                        'moods' => $request['moods'],
                    ]
                ]
            );
        $this
            ->assertDatabaseHas(
                Travel::class,
                [
                    'name' => $request['name'],
                    'description' => $request['description'],
                    'number_of_days' => $request['number_of_days'],
                    'public' => $request['public'],
                    'mood_nature' => $request['moods']['nature'],
                    'mood_relax' => $request['moods']['relax'],
                    'mood_history' => $request['moods']['history'],
                    'mood_culture' => $request['moods']['culture'],
                    'mood_party' => $request['moods']['party'],
                ]
            );
        $spy->shouldHaveReceived('fill');
    }

    public function test_create_travel_by_editor(): void
    {
        $this->changeToEditor();
        $spy = $this
            ->spy(TravelService::class)
            ->makePartial();
        $this
            ->postJson(
                route('travel.store'),
                $request = [
                    'name' => $this->faker->name,
                    'description' => $this->faker->sentence,
                    'number_of_days' => mt_rand(2, 12),
                    'public' => true,
                    'moods' => [
                        'nature' => mt_rand(0, 100),
                        'relax' => mt_rand(0, 100),
                        'history' => mt_rand(0, 100),
                        'culture' => mt_rand(0, 100),
                        'party' => mt_rand(0, 100),
                    ]
                ]
            )
            ->assertUnauthorized();
        $this
            ->assertDatabaseMissing(
                Travel::class,
                [
                    'name' => $request['name'],
                    'description' => $request['description'],
                    'number_of_days' => $request['number_of_days'],
                    'public' => $request['public'],
                    'mood_nature' => $request['moods']['nature'],
                    'mood_relax' => $request['moods']['relax'],
                    'mood_history' => $request['moods']['history'],
                    'mood_culture' => $request['moods']['culture'],
                    'mood_party' => $request['moods']['party'],
                ]
            );
        $spy->shouldNotHaveReceived('fill');
    }

    public function test_update_travel(): void
    {
        $spy = $this
            ->spy(TravelService::class)
            ->makePartial();
        $travel = Travel::factory()->create();
        $this
            ->putJson(
                route('travel.update', $travel),
                $request = [
                    'name' => $this->faker->name,
                    'description' => $this->faker->sentence,
                    'number_of_days' => mt_rand(2, 12),
                    'public' => true,
                    'moods' => [
                        'nature' => mt_rand(0, 100),
                        'relax' => mt_rand(0, 100),
                        'history' => mt_rand(0, 100),
                        'culture' => mt_rand(0, 100),
                        'party' => mt_rand(0, 100),
                    ]
                ]
            )
            ->assertOk()
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'slug',
                        'description',
                        'number_of_days',
                        'number_of_nights',
                        'public',
                        'moods',
                        'created_at',
                        'updated_at',
                    ]
                ]
            )
            ->assertJson(
                [
                    'data' => [
                        'id' => $travel->id,
                        'name' => $request['name'],
                        'slug' => Str::slug($request['name']),
                        'description' => $request['description'],
                        'number_of_days' => $request['number_of_days'],
                        'number_of_nights' => $request['number_of_days'] - 1,
                        'public' => $request['public'],
                        'moods' => $request['moods'],
                    ]
                ]
            );
        $this
            ->assertDatabaseHas(
                Travel::class,
                [
                    'id' => $travel->id,
                    'name' => $request['name'],
                    'description' => $request['description'],
                    'number_of_days' => $request['number_of_days'],
                    'public' => $request['public'],
                    'mood_nature' => $request['moods']['nature'],
                    'mood_relax' => $request['moods']['relax'],
                    'mood_history' => $request['moods']['history'],
                    'mood_culture' => $request['moods']['culture'],
                    'mood_party' => $request['moods']['party'],
                ]
            );
        $spy->shouldHaveReceived('fill');
    }

    public function test_update_travel_by_editor(): void
    {
        $this->changeToEditor();
        $spy = $this
            ->spy(TravelService::class)
            ->makePartial();
        $travel = Travel::factory()->create();
        $this
            ->putJson(
                route('travel.update', $travel),
                $request = [
                    'name' => $this->faker->name,
                    'description' => $this->faker->sentence,
                    'number_of_days' => mt_rand(2, 12),
                    'public' => true,
                    'moods' => [
                        'nature' => mt_rand(0, 100),
                        'relax' => mt_rand(0, 100),
                        'history' => mt_rand(0, 100),
                        'culture' => mt_rand(0, 100),
                        'party' => mt_rand(0, 100),
                    ]
                ]
            )
            ->assertOk()
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'name',
                        'slug',
                        'description',
                        'number_of_days',
                        'number_of_nights',
                        'public',
                        'moods',
                        'created_at',
                        'updated_at',
                    ]
                ]
            )
            ->assertJson(
                [
                    'data' => [
                        'id' => $travel->id,
                        'name' => $request['name'],
                        'slug' => Str::slug($request['name']),
                        'description' => $request['description'],
                        'number_of_days' => $request['number_of_days'],
                        'number_of_nights' => $request['number_of_days'] - 1,
                        'public' => $request['public'],
                        'moods' => $request['moods'],
                    ]
                ]
            );
        $this
            ->assertDatabaseHas(
                Travel::class,
                [
                    'id' => $travel->id,
                    'name' => $request['name'],
                    'description' => $request['description'],
                    'number_of_days' => $request['number_of_days'],
                    'public' => $request['public'],
                    'mood_nature' => $request['moods']['nature'],
                    'mood_relax' => $request['moods']['relax'],
                    'mood_history' => $request['moods']['history'],
                    'mood_culture' => $request['moods']['culture'],
                    'mood_party' => $request['moods']['party'],
                ]
            );
        $spy->shouldHaveReceived('fill');
    }
}
