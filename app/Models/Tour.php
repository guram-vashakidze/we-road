<?php

namespace App\Models;

use App\Casts\MoneyCast;
use App\Filters\TourFilter;
use App\Helpers\Model\HasFilterable;
use App\Helpers\Model\HasUuids;
use Database\Factories\TourFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * @property string $id
 * @property string $travel_id
 * @property string $name
 * @property Carbon $starting_date
 * @property Carbon $ending_date
 * @property float $price
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @see self::travel()
 * @property Travel $travel
 *
 * @method static TourFactory factory($count = null, $state = [])
 */
class Tour extends Model
{
    use HasFactory;
    use HasUuids;
    use HasFilterable;

    protected $fillable = [
        'travel_id',
        'name',
        'starting_date',
        'ending_date',
        'price'
    ];

    protected $casts = [
        'starting_date' => 'date:Y-m-d',
        'ending_date' => 'date:Y-m-d',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'price' => MoneyCast::class,
    ];

    protected static function boot(): void
    {
        parent::boot();

        static::addGlobalScope(
            'default_order',
            static function (Builder $builder): void {
                $builder
                    ->orderBy('starting_date')
                    ->orderBy('id');
            }
        );
    }

    public function travel(): BelongsTo|Travel
    {
        return $this->belongsTo(Travel::class, 'travel_id', 'id');
    }

    public function modelFilter(): string
    {
        return $this->provideFilter(TourFilter::class);
    }

    public function setTravel(Travel $travel): self
    {
        $this->travel_id = $travel->id;
        return $this;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;
        return $this;
    }

    public function setStartingDate(Carbon $date): self
    {
        $this->starting_date = $date;
        return $this;
    }

    public function setEndingDate(Carbon $date): self
    {
        $this->ending_date = $date;
        return $this;
    }
}
