<?php

namespace App\Helpers\Swagger\Enums;

enum SwaggerCollectionFormatEnum: string
{
    public const MULTI = 'multi';
    public const CSV = 'csv';
    public const SSV = 'ssv';
    public const TSV = 'tsv';
    public const PIPES = 'pipes';
}
