<?php

namespace Database\Seeders;

use App\Models\Language;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class LanguageSeeder extends Seeder
{
    private const LANGUAGES = [
        [
            'code' => 'en',
            'name' => 'English',
        ],
    ];

    public function run(): void
    {
        foreach (self::LANGUAGES as $language) {
            Language::insertOrIgnore(
                array_merge(
                    [
                        'id' => Str::orderedUuid()->toString()
                    ],
                    $language
                )
            );
        }
    }
}
