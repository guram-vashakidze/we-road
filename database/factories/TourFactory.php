<?php

namespace Database\Factories;

use App\Models\Tour;
use App\Models\Travel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @extends Factory<Tour>
 *
 * @method Tour|Tour[]|Collection<Tour> create($attributes = [], ?Model $parent = null)
 * @method Tour|Tour[]|Collection<Tour> make($attributes = [], ?Model $parent = null)
 */
class TourFactory extends Factory
{
    protected $model = Tour::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->unique()->name,
            'price' => 999.99,
            'starting_date' => Carbon::now()->addMonth(),
            'ending_date' => Carbon::now()->addMonth()->addDays(10),
            'travel_id' => Travel::factory(['number_of_days' => 10])
        ];
    }

    public function forTravel(Travel $travel): self
    {
        return $this
            ->state(
                [
                    'travel_id' => $travel->id
                ]
            );
    }
}
