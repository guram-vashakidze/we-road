<?php

namespace Tests\Integration\Rules;

use App\Models\Travel;
use App\Rules\TourDatesRule;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class TourDatesRuleTest extends TestCase
{
    use DatabaseTransactions;
    use WithFaker;

    private function mockRequest(Travel $travel): void
    {
        $this
            ->app
            ->bind(
                'request',
                static fn () => new class ($travel) {
                    public function __construct(private readonly Travel $travel)
                    {
                    }

                    public function route(): object
                    {
                        return new class ($this->travel) {
                            public function __construct(private readonly Travel $travel)
                            {
                            }

                            public function parameter(string $parameter): Travel
                            {
                                return $this->travel;
                            }
                        };
                    }
                }
            );
    }

    public function test_passes_validation(): void
    {
        $travel = Travel::factory()->create();
        $this->mockRequest($travel);
        $this->expectNotToPerformAssertions();
        $rule = new TourDatesRule();
        $rule->setData(['starting_date' => Carbon::now()->toDateString()]);
        $rule
            ->validate(
                'ending_date',
                Carbon::now()->addDays($travel->number_of_days)->toDateString(),
                fn (string $message) => $this->assertIsString($message)
            );
    }

    public function test_not_passes_validation_1(): void
    {
        $travel = Travel::factory()->create();
        $this->mockRequest($travel);
        $rule = new TourDatesRule();
        $rule->setData(['starting_date' => Carbon::now()->toDateString()]);
        $rule
            ->validate(
                'ending_date',
                Carbon::now()->addDays($travel->number_of_days - 1)->toDateString(),
                fn (string $message) => $this->assertIsString($message)
            );
    }
}
