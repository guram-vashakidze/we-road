<?php

namespace App\Http\Resources;

use App\Helpers\Swagger\Enums\SwaggerFormatEnum;
use App\Helpers\Swagger\Enums\SwaggerTypeEnum;
use App\Models\Travel;
use Illuminate\Http\Resources\Json\JsonResource;
use OpenApi\Attributes as OAT;

/**
 * @mixin Travel
 */
class TravelResource extends JsonResource
{
    #[
        OAT\Schema(
            schema: "TravelResource",
            required: [
                'id',
                'slug',
                'name',
                'description',
                'number_of_days',
                'number_of_nights',
                'public',
                'moods'
            ],
            properties: [
                new OAT\Property(
                    property: 'id',
                    description: 'Travel ID',
                    type: SwaggerTypeEnum::STRING,
                    format: SwaggerFormatEnum::UUID,
                    example: '550e8400-e29b-41d4-a716-446655440000',
                    nullable: false
                ),
                new OAT\Property(
                    property: 'slug',
                    description: 'Travel slug',
                    type: SwaggerTypeEnum::STRING,
                    example: 'iceland-hunting-northern-lights',
                    nullable: false
                ),
                new OAT\Property(
                    property: 'name',
                    description: 'Travel name',
                    type: SwaggerTypeEnum::STRING,
                    example: 'Iceland: hunting for the Northern Lights',
                    nullable: false
                ),
                new OAT\Property(
                    property: 'description',
                    description: 'Travel description',
                    type: SwaggerTypeEnum::STRING,
                    example: 'Why visit Iceland in winter? Because it is between October and March that this land offers the spectacle of the Northern Lights, one of the most incredible and magical natural phenomena in the world, visible only near the earth\'s two magnetic poles. Come with us on WeRoad to explore this land of ice and fire, full of contrasts and natural variety, where the energy of waterfalls and geysers meets the peace of the fjords... And when the ribbons of light of the aurora borealis twinkle in the sky before our enchanted eyes, we will know that we have found what we were looking for.',
                    nullable: true
                ),
                new OAT\Property(
                    property: 'number_of_days',
                    description: 'Count of days in tour',
                    type: SwaggerTypeEnum::INT,
                    minimum: 1,
                    example: 5,
                    nullable: false
                ),
                new OAT\Property(
                    property: 'number_of_nights',
                    description: 'Count of nights in tour',
                    type: SwaggerTypeEnum::INT,
                    minimum: 0,
                    example: 4,
                    nullable: false
                ),
                new OAT\Property(
                    property: 'public',
                    description: 'Public flag',
                    type: SwaggerTypeEnum::BOOL,
                    example: true,
                    nullable: false
                ),
                new OAT\Property(
                    property: 'moods',
                    description: 'Moods info',
                    required: [
                        'nature',
                        'relax',
                        'history',
                        'culture',
                        'party'
                    ],
                    properties: [
                        new OAT\Property(
                            property: 'nature',
                            type: SwaggerTypeEnum::INT,
                            minimum: 0,
                            example: 10,
                            nullable: false
                        ),
                        new OAT\Property(
                            property: 'relax',
                            type: SwaggerTypeEnum::INT,
                            minimum: 0,
                            example: 5,
                            nullable: false
                        ),
                        new OAT\Property(
                            property: 'history',
                            type: SwaggerTypeEnum::INT,
                            minimum: 0,
                            example: 40,
                            nullable: false
                        ),
                        new OAT\Property(
                            property: 'culture',
                            type: SwaggerTypeEnum::INT,
                            minimum: 0,
                            example: 20,
                            nullable: false
                        ),
                        new OAT\Property(
                            property: 'party',
                            type: SwaggerTypeEnum::INT,
                            minimum: 0,
                            example: 15,
                            nullable: false
                        ),
                    ],
                    type: SwaggerTypeEnum::OBJECT,
                    nullable: false
                ),
                new OAT\Property(
                    property: 'created_at',
                    type: SwaggerTypeEnum::STRING,
                    format: SwaggerFormatEnum::DATETIME,
                    example: '2024-01-01 10:00:00',
                    nullable: false
                ),
                new OAT\Property(
                    property: 'updated_at',
                    type: SwaggerTypeEnum::STRING,
                    format: SwaggerFormatEnum::DATETIME,
                    example: '2024-01-01 10:00:00',
                    nullable: false
                ),
            ]
        )
    ]
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'name' => $this->name,
            'description' => $this->description,
            'number_of_days' => $this->number_of_days,
            'number_of_nights' => $this->number_of_days - 1,
            'public' => $this->public,
            'moods' => $this->moods,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }
}
