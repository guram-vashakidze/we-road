<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class TourSeeder extends Seeder
{
    public function run(): void
    {
        $tours = json_decode(
            file_get_contents(storage_path('samples/tours.json')),
            true
        );
        foreach ($tours as $tour) {
            DB::table('tours')
                ->updateOrInsert(
                    [
                        'id' => $tour['id']
                    ],
                    [
                        'name' => $tour['name'],
                        'travel_id' =>  $tour['travelId'],
                        'starting_date' => $tour['startingDate'],
                        'ending_date' => $tour['endingDate'],
                        'price' => $tour['price'],
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString(),
                    ]
                );
        }
    }
}
