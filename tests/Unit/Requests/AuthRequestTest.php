<?php

namespace Tests\Unit\Requests;

use App\Http\Requests\AuthRequest;
use Faker\Factory;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Validator;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;

class AuthRequestTest extends TestCase
{
    use WithFaker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setUpFaker();
    }

    /**
     * @param string $email
     * @param string $password
     * @param bool $passes
     * @param array|null $fields
     * @return void
     * @throws Exception
     * @dataProvider data
     */
    public function test_validation_cases(string $email, string $password, bool $passes, array $fields = null): void
    {
        $request = AuthRequest::create(
            Request::METHOD_POST,
            $this->faker->url(),
            [
                'email' => $email,
                'password' => $password
            ]
        );
        $validator = new Validator(
            $this->createMock(Translator::class),
            $request->all(),
            $request->rules()
        );
        $this->assertEquals($passes, $validator->passes());
        if ($fields === null) {
            return;
        }
        $this->assertEquals(
            $fields,
            array_keys($validator->errors()->toArray())
        );
    }

    public static function data(): array
    {
        $faker = Factory::create();
        return [
            [
                $faker->email,
                $faker->bothify('#?#?#?#?#'),
                true
            ],
            [
                $faker->email,
                $faker->bothify('#?#?#?'),
                true
            ],
            [
                $faker->word,
                $faker->bothify('#?#?#?#?#'),
                false,
                [
                    'email'
                ]
            ],
            [
                $faker->numerify,
                $faker->bothify('#?#?#?'),
                false,
                [
                    'email'
                ]
            ],
            [
                $faker->email,
                $faker->bothify('#?#'),
                false,
                [
                    'password'
                ]
            ],
            [
                $faker->word,
                $faker->bothify('#?#'),
                false,
                [
                    'email',
                    'password'
                ]
            ],
        ];
    }
}
