<?php

namespace App\Helpers\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Str;

trait HasUuids
{
    use \Illuminate\Database\Eloquent\Concerns\HasUuids;

    public function newUniqueId(): string
    {
        return Str::orderedUuid()->toString();
    }

    /**
     * @param $query
     * @param $value
     * @param $field
     * @return Relation|Builder
     */
    public function resolveRouteBindingQuery($query, $value, $field = null): Relation|Builder
    {
        if ($field && in_array($field, $this->uniqueIds()) && !Str::isUuid($value)) {
            throw (new ModelNotFoundException())->setModel(get_class($this), $value);
        }

        if (!$field && in_array($this->getRouteKeyName(), $this->uniqueIds()) && !Str::isUuid($value)) {
            throw (new ModelNotFoundException())->setModel(get_class($this), $value);
        }

        return parent::resolveRouteBindingQuery($query, $value, $field);
    }
}
