<?php

namespace Tests\Unit\Casts;

use App\Casts\MoneyCast;
use Illuminate\Database\Eloquent\Model;
use PHPUnit\Framework\TestCase;

class MoneyCastTest extends TestCase
{
    public function test_get(): void
    {
        $this->assertEquals(
            100.00,
            (new MoneyCast())
                ->get(
                    new class () extends Model {},
                    'price',
                    10000,
                    []
                )
        );
    }

    public function test_set(): void
    {
        $this->assertEquals(
            10000,
            (new MoneyCast())
                ->set(
                    new class () extends Model {},
                    'price',
                    100.00,
                    []
                )
        );
    }
}
