<?php

namespace App\Helpers\Model;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Builder;

/**
 * @method static Builder|static filter($query, array $input = [], $filter = null)
 */
trait HasFilterable
{
    use Filterable;
}
