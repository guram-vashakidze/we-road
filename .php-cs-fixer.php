<?php

$finder = PhpCsFixer\Finder::create()
    ->ignoreUnreadableDirs()
    ->in(__DIR__ . "/app")
    ->in(__DIR__ . "/config")
    ->in(__DIR__ . "/database")
    ->in(__DIR__ . "/resources")
    ->in(__DIR__ . "/tests");

$config = new PhpCsFixer\Config();
return $config->setRules([
    '@PSR12' => true,
])->setFinder($finder);
