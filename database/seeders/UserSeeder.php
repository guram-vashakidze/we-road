<?php

namespace Database\Seeders;

use App\Enums\RoleEnum;
use App\Models\Language;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    private const USERS = [
        [
            'email' => 'admin@we-road.com',
            'password' => '12345678',
            'role' => RoleEnum::ADMIN
        ],
        [
            'email' => 'editor@we-road.com',
            'password' => '12345678',
            'role' => RoleEnum::EDITOR
        ]
    ];

    public function run(): void
    {
        $language = Language::where('code', 'en')->first();
        foreach (self::USERS as $user) {
            $role = Role::where('name', $user['role'])->first()->id;
            User::updateOrInsert(
                [
                    'email' => $user['email'],
                ],
                [
                    'id' => Str::orderedUuid()->toString(),
                    'password' => Hash::make($user['password']),
                    'language_id' => $language->id,
                    'role_id' => $role,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                ]
            );
        }
    }
}
