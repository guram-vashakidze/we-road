<?php

use App\Enums\RoleEnum;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TourController;
use App\Http\Controllers\TravelController;
use App\Http\Middleware\PublicTravelMiddleware;
use Symfony\Component\HttpFoundation\Response;

Route::prefix('auth')
    ->name('auth.')
    ->group(
        static function (): void {
            Route::post('login', [AuthController::class, 'login'])
                ->name('login');
            Route::middleware('auth:api')
                ->post('logout', [AuthController::class, 'logout'])
                ->name('logout');
        }
    );

Route::prefix('travel')
    ->name('travel.')
    ->middleware('auth:api')
    ->group(
        static function (): void {
            Route::get('{travel}', [TravelController::class, 'show'])
                ->name('show');
            Route::middleware('role:' . RoleEnum::ADMIN->value)
                ->post('', [TravelController::class, 'store'])
                ->name('store');
            Route::middleware('role:' . RoleEnum::ADMIN->value . ',' . RoleEnum::EDITOR->value)
                ->put('{travel}', [TravelController::class, 'update'])
                ->name('update');
            Route::prefix('{travel}/tour')
                ->name('tour.')
                ->group(
                    static function (): void {
                        Route::get('{tour}', [TourController::class, 'show'])
                            ->name('show');
                        Route::middleware('role:' . RoleEnum::ADMIN->value)
                            ->post('', [TourController::class, 'store'])
                            ->name('store');
                    }
                );
        }
    );

Route::middleware(PublicTravelMiddleware::class)
    ->get('travel/{travel}/tour', [TourController::class, 'index'])
    ->name('travel.tour.index');

Route::prefix('redoc')
    ->group(
        static function (): void {
            Route::get('', static fn () => view('vendor.redoc.index'));
            Route::get(
                'api.json',
                static function () {
                    $path = storage_path('api-docs/api-docs.json');
                    if (!file_exists($path)) {
                        abort(Response::HTTP_NOT_FOUND);
                    }
                    return response()
                        ->json(
                            json_decode(
                                file_get_contents($path),
                                true
                            )
                        );
                }
            );
        }
    );
