<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Testing\TestResponse;
use Tests\Contracts\WithoutAuthorization;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function setUp(): void
    {
        if (method_exists($this, 'authorize') && !$this instanceof WithoutAuthorization) {
            $this->afterApplicationCreated(
                fn () => $this->authorize()
            );
        }
        parent::setUp();
    }

    private function makeHeaders(array $headers): array
    {
        if (!method_exists($this, 'authHeader')) {
            return $headers;
        }
        return array_merge($headers, $this->authHeader());
    }

    public function getJson($uri, array $headers = [], $options = 0): TestResponse
    {
        return parent::getJson($uri, $this->makeHeaders($headers), $options);
    }

    public function postJson($uri, array $data = [], array $headers = [], $options = 0): TestResponse
    {
        return parent::postJson($uri, $data, $this->makeHeaders($headers), $options);
    }

    public function putJson($uri, array $data = [], array $headers = [], $options = 0): TestResponse
    {
        return parent::putJson($uri, $data, $this->makeHeaders($headers), $options);
    }
}
