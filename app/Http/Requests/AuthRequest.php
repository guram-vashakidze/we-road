<?php

namespace App\Http\Requests;

use App\Dto\AuthDto;
use App\Helpers\Swagger\Enums\SwaggerFormatEnum;
use App\Helpers\Swagger\Enums\SwaggerTypeEnum;
use OpenApi\Attributes as OAT;

class AuthRequest extends BaseFormRequest
{
    #[
        OAT\Schema(
            schema: "AuthRequest",
            required: ['email', 'password'],
            properties: [
                new OAT\Property(
                    property: 'email',
                    description: 'User email',
                    type: SwaggerTypeEnum::STRING,
                    format: SwaggerFormatEnum::EMAIL,
                    example: 'admin@we-road.com',
                    nullable: false
                ),
                new OAT\Property(
                    property: 'password',
                    description: 'User password',
                    type: SwaggerTypeEnum::STRING,
                    minLength: 6,
                    example: '12345678',
                    nullable: false
                ),
            ]
        )
    ]
    public function rules(): array
    {
        return [
            'email' => [
                'required',
                'string',
                'email'
            ],
            'password' => [
                'required',
                'string',
                'min:6'
            ]
        ];
    }

    public function toDto(): AuthDto
    {
        return AuthDto::init($this->validated());
    }
}
