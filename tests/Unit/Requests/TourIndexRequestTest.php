<?php

namespace Tests\Unit\Requests;

use App\Http\Requests\TourIndexRequest;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Validator;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use Tests\CreatesApplication;

class TourIndexRequestTest extends TestCase
{
    use WithFaker;
    use CreatesApplication;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setUpFaker();
        $this->createApplication();
    }

    /**
     * @param array $body
     * @param bool $passes
     * @param array|null $fields
     * @return void
     * @throws Exception
     * @dataProvider data
     */
    public function test_validation_cases(array $body, bool $passes, ?array $fields = null): void
    {
        $request = TourIndexRequest::create(
            Request::METHOD_GET,
            $this->faker->url,
            $body
        );
        $validator = new Validator(
            $this->createMock(Translator::class),
            $request->all(),
            $request->rules()
        );
        $this->assertEquals($passes, $validator->passes());
        if ($fields === null) {
            return;
        }
        $this->assertEquals(
            $fields,
            array_keys($validator->errors()->toArray())
        );
    }

    public static function data(): array
    {
        return [
            [
                [],
                true
            ],
            [
                [
                    'date_from' => Carbon::now()->addDays(10)->toDateString(),
                    'price_from' => 100.0
                ],
                true
            ],
            [
                [
                    'date_to' => Carbon::now()->addDays(10)->toDateString(),
                    'price_to' => 100.0
                ],
                true
            ],
            [
                [
                    'order' => [
                        'column' => 'price',
                        'direction' => 'desc'
                    ]
                ],
                true
            ],
            [
                [
                    'order' => [
                        'column' => 'price',
                    ]
                ],
                true
            ],
            [
                [
                    'date_from' => Carbon::now()->addDays(10)->toDateString(),
                    'date_to' => Carbon::now()->addDays(5)->toDateString(),
                ],
                false,
                [
                    'date_to'
                ]
            ],
            [
                [
                    'price_from' => 100,
                    'price_to' => 50,
                ],
                false,
                [
                    'price_to'
                ]
            ],
            [
                [
                    'order' => [
                        'direction' => 'desc',
                    ]
                ],
                false,
                [
                    'order.column'
                ]
            ],
            [
                [
                    'order' => [
                        'column' => 'foo',
                        'direction' => 'asc',
                    ]
                ],
                false,
                [
                    'order.column'
                ]
            ],
        ];
    }
}
