<?php

namespace Database\Factories;

use App\Models\Travel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;

/**
 * @extends Factory<Travel>
 *
 * @method Travel|Travel[]|Collection<Travel> create($attributes = [], ?Model $parent = null)
 * @method Travel|Travel[]|Collection<Travel> make($attributes = [], ?Model $parent = null)
 */
class TravelFactory extends Factory
{
    protected $model = Travel::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'description' => $this->faker->sentence,
            'slug' => $this->faker->slug,
            'number_of_days' => mt_rand(2, 12),
            'public' => true,
            'mood_nature' => mt_rand(0, 100),
            'mood_relax' => mt_rand(0, 100),
            'mood_history' => mt_rand(0, 100),
            'mood_culture' => mt_rand(0, 100),
            'mood_party' => mt_rand(0, 100),
        ];
    }
}
