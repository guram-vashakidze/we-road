<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create(
            'travels',
            static function (Blueprint $table): void {
                $table
                    ->uuid('id')
                    ->primary();
                $table
                    ->string('slug')
                    ->unique();
                $table
                    ->text('name');
                $table
                    ->longText('description')
                    ->nullable();
                $table
                    ->integer('number_of_days');
                $table
                    ->integer('mood_nature')
                    ->default(0);
                $table
                    ->integer('mood_relax')
                    ->default(0);
                $table
                    ->integer('mood_history')
                    ->default(0);
                $table
                    ->integer('mood_culture')
                    ->default(0);
                $table
                    ->integer('mood_party')
                    ->default(0);
                $table
                    ->boolean('public')
                    ->default(1);
                $table
                    ->timestamps();
            }
        );
    }

    public function down(): void
    {
        Schema::dropIfExists('travels');
    }
};
