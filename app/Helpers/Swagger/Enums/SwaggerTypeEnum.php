<?php

namespace App\Helpers\Swagger\Enums;

enum SwaggerTypeEnum: string
{
    public const INT = 'integer';
    public const STRING = 'string';
    public const BOOL = 'boolean';
    public const NUMBER = 'number';
    public const ARRAY = 'array';
    public const OBJECT = 'object';
}
