<?php

namespace App\Services;

use App\Dto\AuthDto;
use App\Exceptions\UnauthorizedException;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthService
{
    public const GRAND_TOKEN_NAME = 'grand_auth_token';
    public const PERSONAL_TOKEN_NAME = 'personal_auth_token';

    /**
     * @param AuthDto $dto
     * @return string
     * @throws UnauthorizedException
     */
    public function login(AuthDto $dto): string
    {
        $user = User::forEmail($dto->email)->first();
        if (!$user) {
            throw new UnauthorizedException();
        }
        if (!Hash::check($dto->password, $user->password)) {
            throw new UnauthorizedException();
        }
        return $user->createToken(self::PERSONAL_TOKEN_NAME)->accessToken;
    }

    public function logout(User $user): void
    {
        $token = $user->token();
        if (!$token) {
            return;
        }
        $token->revoke();
    }
}
