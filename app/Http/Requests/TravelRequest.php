<?php

namespace App\Http\Requests;

use App\Dto\TravelDto;
use App\Helpers\Swagger\Enums\SwaggerTypeEnum;
use App\Rules\TravelUniqRule;
use OpenApi\Attributes as OAT;

class TravelRequest extends BaseFormRequest
{
    #[
        OAT\Schema(
            schema: "TravelRequest",
            required: [
                'name',
                'number_of_days',
                'public',
                'moods'
            ],
            properties: [
                new OAT\Property(
                    property: 'name',
                    description: 'Travel name',
                    type: SwaggerTypeEnum::STRING,
                    example: 'Iceland: hunting for the Northern Lights',
                    nullable: false
                ),
                new OAT\Property(
                    property: 'description',
                    description: 'Travel description',
                    type: SwaggerTypeEnum::STRING,
                    example: 'Why visit Iceland in winter? Because it is between October and March that this land offers the spectacle of the Northern Lights, one of the most incredible and magical natural phenomena in the world, visible only near the earth\'s two magnetic poles. Come with us on WeRoad to explore this land of ice and fire, full of contrasts and natural variety, where the energy of waterfalls and geysers meets the peace of the fjords... And when the ribbons of light of the aurora borealis twinkle in the sky before our enchanted eyes, we will know that we have found what we were looking for.',
                    nullable: true
                ),
                new OAT\Property(
                    property: 'number_of_days',
                    description: 'Count of days in tour',
                    type: SwaggerTypeEnum::INT,
                    minimum: 1,
                    example: 5,
                    nullable: false
                ),
                new OAT\Property(
                    property: 'public',
                    description: 'Public flag',
                    type: SwaggerTypeEnum::BOOL,
                    example: true,
                    nullable: false
                ),
                new OAT\Property(
                    property: 'moods',
                    description: 'Moods info',
                    required: [
                        'nature',
                        'relax',
                        'history',
                        'culture',
                        'party'
                    ],
                    properties: [
                        new OAT\Property(
                            property: 'nature',
                            type: SwaggerTypeEnum::INT,
                            minimum: 0,
                            example: 10,
                            nullable: false
                        ),
                        new OAT\Property(
                            property: 'relax',
                            type: SwaggerTypeEnum::INT,
                            minimum: 0,
                            example: 5,
                            nullable: false
                        ),
                        new OAT\Property(
                            property: 'history',
                            type: SwaggerTypeEnum::INT,
                            minimum: 0,
                            example: 40,
                            nullable: false
                        ),
                        new OAT\Property(
                            property: 'culture',
                            type: SwaggerTypeEnum::INT,
                            minimum: 0,
                            example: 20,
                            nullable: false
                        ),
                        new OAT\Property(
                            property: 'party',
                            type: SwaggerTypeEnum::INT,
                            minimum: 0,
                            example: 15,
                            nullable: false
                        ),
                    ],
                    type: SwaggerTypeEnum::OBJECT,
                    nullable: false
                ),
            ]
        )
    ]
    public function rules(): array
    {
        return [
            'name' => [
                'required',
                'string',
                new TravelUniqRule()
            ],
            'description' => [
                'nullable',
                'string'
            ],
            'number_of_days' => [
                'required',
                'int',
                'min:1'
            ],
            'public' => [
                'bool'
            ],
            'moods' => [
                'required',
                'array'
            ],
            'moods.nature' => [
                'required',
                'int',
                'min:0'
            ],
            'moods.relax' => [
                'required',
                'int',
                'min:0'
            ],
            'moods.history' => [
                'required',
                'int',
                'min:0'
            ],
            'moods.culture' => [
                'required',
                'int',
                'min:0'
            ],
            'moods.party' => [
                'required',
                'int',
                'min:0'
            ],
        ];
    }

    public function toDto(): TravelDto
    {
        return TravelDto::init($this->validated());
    }
}
