<?php

namespace Database\Seeders;

use App\Services\AuthService;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class PasswordSeeder extends Seeder
{
    public function run(): void
    {
        if (DB::table('oauth_clients')->count()) {
            return;
        }
        $provider = in_array('users', array_keys(config('auth.providers'))) ? 'users' : null;
        Artisan::call('passport:keys', ['--force' => true]);
        Artisan::call('passport:client', ['--personal' => true, '--name' => AuthService::PERSONAL_TOKEN_NAME]);
        Artisan::call('passport:client', ['--password' => true, '--name' => AuthService::GRAND_TOKEN_NAME, '--provider' => $provider]);
    }
}
