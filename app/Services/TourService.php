<?php

namespace App\Services;

use App\Dto\TourDto;
use App\Exceptions\SomethingWentWrongException;
use App\Models\Tour;
use App\Models\Travel;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

class TourService
{
    /**
     * @param TourDto $dto
     * @param Travel $travel
     * @param Tour $tour
     * @return Tour
     * @throws SomethingWentWrongException
     * @throws Throwable
     */
    public function fill(TourDto $dto, Travel $travel, Tour $tour): Tour
    {
        try {
            DB::beginTransaction();
            $tour
                ->setTravel($travel)
                ->setName($dto->name)
                ->setPrice($dto->price)
                ->setStartingDate($dto->startingDate)
                ->setEndingDate($dto->endingDate)
                ->save();
            DB::commit();
            return $tour;
        } catch (Throwable $e) {
            DB::rollBack();
            Log::error($e);
            throw new SomethingWentWrongException();
        }
    }

    public function list(Travel $travel, array $filter, int $page, int $perPage): LengthAwarePaginator
    {
        return $travel
            ->tours()
            ->select(['id', 'name', 'starting_date', 'ending_date', 'price'])
            ->filter($filter)
            ->paginate(perPage: $perPage, page: $page);
    }
}
