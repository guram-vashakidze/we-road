<?php

namespace Tests\Integration\Middlewares;

use App\Http\Middleware\PublicTravelMiddleware;
use App\Models\Travel;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class PublicTravelMiddlewareTest extends TestCase
{
    use DatabaseTransactions;

    public function test_access_to_public_travel(): void
    {
        $travel = Travel::factory()->create();
        $request = new Request();
        $request
            ->setRouteResolver(
                static fn ($guard = null) => new class ($travel) {
                    public function __construct(private readonly Travel $travel)
                    {
                    }

                    public function parameter(): Travel
                    {
                        return $this->travel;
                    }
                }
            );
        $response = (new PublicTravelMiddleware())
            ->handle(
                $request,
                static fn (Request $request): Response => response()->json()
            );

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function test_access_to_not_public_travel(): void
    {
        $travel = Travel::factory(['public' => false])->create();
        $request = new Request();
        $request
            ->setRouteResolver(
                static fn ($guard = null) => new class ($travel) {
                    public function __construct(private readonly Travel $travel)
                    {
                    }

                    public function parameter(): Travel
                    {
                        return $this->travel;
                    }
                }
            );
        $response = (new PublicTravelMiddleware())
            ->handle(
                $request,
                static fn (Request $request): Response => response()->json()
            );

        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    public function test_access_to_not_public_travel_under_auth(): void
    {
        $travel = Travel::factory(['public' => false])->create();
        $request = new Request();
        $request
            ->setRouteResolver(
                static fn ($guard = null): object => new class ($travel) {
                    public function __construct(private readonly Travel $travel)
                    {
                    }

                    public function parameter(): Travel
                    {
                        return $this->travel;
                    }
                }
            )
            ->setUserResolver(
                static fn (): User => User::factory()->create()
            );
        $response = (new PublicTravelMiddleware())
            ->handle(
                $request,
                static fn (Request $request): Response => response()->json()
            );

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }
}
