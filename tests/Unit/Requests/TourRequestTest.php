<?php

namespace Tests\Unit\Requests;

use App\Http\Requests\TourRequest;
use Faker\Factory;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Validator;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use Tests\CreatesApplication;

class TourRequestTest extends TestCase
{
    use WithFaker;
    use CreatesApplication;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setUpFaker();
        $this->createApplication();
    }

    /**
     * @param array $body
     * @param bool $passes
     * @param array|null $fields
     * @return void
     * @throws Exception
     * @dataProvider data
     */
    public function test_validation_cases(array $body, bool $passes, ?array $fields = null): void
    {
        $request = TourRequest::create(
            Request::METHOD_POST,
            $this->faker->url,
            $body
        );
        $validator = new Validator(
            $this->createMock(Translator::class),
            $request->all(),
            $request->rules()
        );
        $this->assertEquals($passes, $validator->passes());
        if ($fields === null) {
            return;
        }
        $this->assertEquals(
            $fields,
            array_keys($validator->errors()->toArray())
        );
    }

    public static function data(): array
    {
        $faker = Factory::create();
        return [
            [
                [
                    'name' => $faker->name,
                    'starting_date' => Carbon::now()->toDateString(),
                    'ending_date' => Carbon::now()->addDays(10)->toDateString(),
                    'price' => 100,
                ],
                true
            ],
            [
                [
                    'name' => $faker->name,
                    'starting_date' => Carbon::now()->subDay()->toDateString(),
                    'ending_date' => Carbon::now()->addDays(10)->toDateString(),
                    'price' => 100,
                ],
                false,
                [
                    'starting_date'
                ]
            ],
            [
                [
                    'name' => $faker->name,
                    'starting_date' => Carbon::now()->toDateString(),
                    'ending_date' => Carbon::now()->toDateString(),
                    'price' => 100,
                ],
                false,
                [
                    'ending_date'
                ]
            ],
            [
                [
                    'name' => $faker->name,
                    'starting_date' => Carbon::now()->toDateTimeString(),
                    'ending_date' => Carbon::now()->addDays(10)->toDateTimeString(),
                    'price' => 100,
                ],
                false,
                [
                    'starting_date',
                    'ending_date'
                ]
            ],
            [
                [
                    'name' => $faker->name,
                    'starting_date' => Carbon::now()->toDateString(),
                    'ending_date' => Carbon::now()->addDays(10)->toDateString(),
                    'price' => 0,
                ],
                false,
                [
                    'price'
                ]
            ],
        ];
    }
}
