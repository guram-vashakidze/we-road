<?php

namespace App\Dto;

/**
 * @property-read string $email
 * @property-read string $password
 */
class AuthDto extends BaseDto
{
    protected string $email;
    protected string $password;

    public static function init(array $args): self
    {
        $dto = new self();
        $dto->email = $args['email'];
        $dto->password = $args['password'];
        return $dto;
    }
}
