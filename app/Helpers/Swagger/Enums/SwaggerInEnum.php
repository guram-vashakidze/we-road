<?php

namespace App\Helpers\Swagger\Enums;

enum SwaggerInEnum: string
{
    public const PATH = 'path';
    public const QUERY = 'query';
    public const BODY = 'body';
    public const HEADER = 'header';
}
