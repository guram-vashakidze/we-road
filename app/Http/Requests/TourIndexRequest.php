<?php

namespace App\Http\Requests;

use Closure;
use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\ValidationRule;

class TourIndexRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'date_from' => [
                'date_format:Y-m-d',
            ],
            'date_to' => [
                'date_format:Y-m-d',
                'after_or_equal:date_from',
            ],
            'price_from' => [
                'numeric',
                'gte:0'
            ],
            'price_to' => [
                'numeric',
                'gt:0',
                new class () implements ValidationRule, DataAwareRule {
                    private ?float $priceFrom;

                    public function setData(array $data): void
                    {
                        $this->priceFrom = !empty($data['price_from']) ? (float) $data['price_from'] : null;
                    }

                    public function validate(string $attribute, mixed $value, Closure $fail): void
                    {
                        if ($this->priceFrom === null || $this->priceFrom <= (float) $value) {
                            return;
                        }
                        $fail(trans('validation.gte.numeric', ['attribute' => $attribute, 'value' => $this->priceFrom]));
                    }
                }
            ],
            'order' => [
                'array',
            ],
            'order.direction' => [
                'string',
                'in:asc,desc'
            ],
            'order.column' => [
                'required_with:order',
                'string',
                'in:price'
            ],
            'page' => [
                'int',
                'gt:0'
            ],
            'per_page' => [
                'int',
                'gt:0'
            ]
        ];
    }

    public function getFilter(): array
    {
        return $this
            ->only(
                [
                    'date_from',
                    'date_to',
                    'price_from',
                    'price_to',
                    'order'
                ]
            );
    }

    public function getPage(): int
    {
        return $this->validated('page', 1);
    }

    public function getPerPage(): int
    {
        return $this->validated('per_page', config('eloquentfilter.paginate_limit'));
    }
}
