<?php

namespace Tests\Helpers;

use App\Enums\RoleEnum;
use App\Models\Role;
use App\Models\User;
use App\Services\AuthService;

trait AuthorizationHelper
{
    protected User $user;
    protected string $token;

    public function authorize(): void
    {
        $this->user = User::factory()
            ->create();
        $this->token = $this
            ->user
            ->createToken(AuthService::PERSONAL_TOKEN_NAME)
            ->accessToken;
    }

    public function changeToEditor(): void
    {
        $this->user->role_id = Role::where('name', RoleEnum::EDITOR->value)->value('id');
        $this->user->save();
    }

    public function authHeader(): array
    {
        if (!isset($this->token)) {
            return [];
        }
        return [
            'Authorization' => 'Bearer ' . $this->token
        ];
    }
}
