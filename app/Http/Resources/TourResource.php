<?php

namespace App\Http\Resources;

use App\Helpers\Swagger\Enums\SwaggerFormatEnum;
use App\Helpers\Swagger\Enums\SwaggerTypeEnum;
use App\Models\Tour;
use Illuminate\Http\Resources\Json\JsonResource;
use OpenApi\Attributes as OAT;

/**
 * @mixin Tour
 */
class TourResource extends JsonResource
{
    #[
        OAT\Schema(
            schema: "TourResource",
            required: [
                'id',
                'name',
                'starting_date',
                'ending_date',
                'price',
            ],
            properties: [
                new OAT\Property(
                    property: 'id',
                    description: 'Tour ID',
                    type: SwaggerTypeEnum::STRING,
                    format: SwaggerFormatEnum::UUID,
                    example: '550e8400-e29b-41d4-a716-446655440000',
                    nullable: false
                ),
                new OAT\Property(
                    property: 'name',
                    description: 'Tour name',
                    type: SwaggerTypeEnum::STRING,
                    example: 'ITJOR20211125',
                    nullable: false
                ),
                new OAT\Property(
                    property: 'starting_date',
                    type: SwaggerTypeEnum::STRING,
                    format: SwaggerFormatEnum::DATE,
                    example: '2024-01-01',
                    nullable: false
                ),
                new OAT\Property(
                    property: 'ending_date',
                    type: SwaggerTypeEnum::STRING,
                    format: SwaggerFormatEnum::DATE,
                    example: '2024-01-10',
                    nullable: false
                ),
                new OAT\Property(
                    property: 'price',
                    type: SwaggerTypeEnum::NUMBER,
                    format: SwaggerFormatEnum::NUMERIC,
                    example: '100.00',
                    nullable: false
                ),
                new OAT\Property(
                    property: 'created_at',
                    type: SwaggerTypeEnum::STRING,
                    format: SwaggerFormatEnum::DATETIME,
                    example: '2024-01-01 10:00:00',
                    nullable: true
                ),
                new OAT\Property(
                    property: 'updated_at',
                    type: SwaggerTypeEnum::STRING,
                    format: SwaggerFormatEnum::DATETIME,
                    example: '2024-01-01 10:00:00',
                    nullable: true
                ),
            ]
        )
    ]
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'starting_date' => $this->starting_date->toDateString(),
            'ending_date' => $this->ending_date->toDateString(),
            'price' => $this->price,
            'created_at' => $this->created_at?->toDateTimeString(),
            'updated_at' => $this->updated_at?->toDateTimeString(),
        ];
    }
}
