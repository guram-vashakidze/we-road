<?php

namespace App\Rules;

use App\Models\Travel;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Translation\PotentiallyTranslatedString;

readonly class TravelUniqRule implements ValidationRule
{
    /**
     * @param Closure(string): PotentiallyTranslatedString $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        /**@var Travel|null $travel*/
        $travel = request()
            ->route()
            ?->parameter('travel');
        $query = Travel::where($attribute, $value);
        if ($travel) {
            $query->where('id', '<>', $travel->id);
        }
        if ($query->doesntExist()) {
            return;
        }
        $fail(trans('error.uniq_travel', ['attribute' => $attribute, 'value' => $value]));
    }
}
