<?php

namespace App\Helpers\Swagger\Enums;

enum SwaggerFormatEnum: string
{
    public const DATE = 'date';
    public const DATETIME = 'date-time';
    public const PASSWORD = 'password';
    public const BYTE = 'byte';
    public const BINARY = 'binary';
    public const EMAIL = 'email';
    public const UUID = 'uuid';
    public const URI = 'uri';
    public const HOSTNAME = 'hostname';
    public const IPV4 = 'ipv4';
    public const IPV6 = 'ipv6';
    public const NUMERIC = 'numeric';
}
