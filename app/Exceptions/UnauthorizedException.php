<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class UnauthorizedException extends Exception implements Throwable
{
    public function __construct()
    {
        parent::__construct(trans('auth.failed'));
    }
}
