<?php

namespace Tests\Integration\Services;

use App\Dto\TravelDto;
use App\Models\Travel;
use App\Services\TravelService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TravelServiceTest extends TestCase
{
    use DatabaseTransactions;
    use WithFaker;

    private TravelService $service;

    public function setUp(): void
    {
        parent::setUp();
        $this->service = resolve(TravelService::class);
    }

    public function test_create_travel(): void
    {
        $travel = $this
            ->service
            ->fill(
                $dto = TravelDto::init(
                    [
                        'name' => $this->faker->name,
                        'description' => $this->faker->sentence,
                        'number_of_days' => mt_rand(2, 10),
                        'public' => true,
                        'moods' => [
                            'nature' => mt_rand(0, 30),
                            'relax' => mt_rand(0, 30),
                            'history' => mt_rand(0, 30),
                            'culture' => mt_rand(0, 30),
                            'party' => mt_rand(0, 30),
                        ]
                    ]
                ),
                Travel::make()
            );
        $this->assertInstanceOf(Travel::class, $travel);
        $this
            ->assertDatabaseHas(
                Travel::class,
                [
                    'id' => $travel->id,
                    'name' => $dto->name,
                    'description' => $dto->description,
                    'number_of_days' => $dto->numberOfDays,
                    'public' => $dto->public,
                    'mood_nature' => $dto->moods['nature'],
                    'mood_relax' => $dto->moods['relax'],
                    'mood_history' => $dto->moods['history'],
                    'mood_culture' => $dto->moods['culture'],
                    'mood_party' => $dto->moods['party'],
                ]
            );
    }

    public function test_update_travel(): void
    {
        $travel = Travel::factory()
            ->create();
        $result = $this
            ->service
            ->fill(
                $dto = TravelDto::init(
                    [
                        'name' => $this->faker->name,
                        'description' => $this->faker->sentence,
                        'number_of_days' => mt_rand(2, 10),
                        'public' => true,
                        'moods' => [
                            'nature' => mt_rand(0, 30),
                            'relax' => mt_rand(0, 30),
                            'history' => mt_rand(0, 30),
                            'culture' => mt_rand(0, 30),
                            'party' => mt_rand(0, 30),
                        ]
                    ]
                ),
                $travel
            );
        $this->assertInstanceOf(Travel::class, $travel);
        $this->assertEquals($travel->id, $result->id);
        $this
            ->assertDatabaseHas(
                Travel::class,
                [
                    'id' => $travel->id,
                    'name' => $dto->name,
                    'description' => $dto->description,
                    'number_of_days' => $dto->numberOfDays,
                    'public' => $dto->public,
                    'mood_nature' => $dto->moods['nature'],
                    'mood_relax' => $dto->moods['relax'],
                    'mood_history' => $dto->moods['history'],
                    'mood_culture' => $dto->moods['culture'],
                    'mood_party' => $dto->moods['party'],
                ]
            );
    }
}
