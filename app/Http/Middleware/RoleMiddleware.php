<?php

namespace App\Http\Middleware;

use App\Http\Resources\ErrorResource;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

readonly class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Closure(Request): (Response) $next
     */
    public function handle(Request $request, Closure $next, string ...$roles): Response
    {
        if (!$request->user()) {
            return $this->returnUnauthorized($request);
        }
        $userRole = $request->user()->role->name->value;
        if (!in_array($userRole, $roles)) {
            return $this->returnUnauthorized($request);
        }
        return $next($request);
    }

    private function returnUnauthorized(Request $request): Response
    {
        return response()
            ->json(
                ErrorResource::make(
                    [
                        'error' => 'Unauthorized',
                        'code' => Response::HTTP_UNAUTHORIZED
                    ]
                )
                    ->toArray($request),
                Response::HTTP_UNAUTHORIZED
            );
    }
}
