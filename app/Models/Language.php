<?php

namespace App\Models;

use App\Helpers\Model\HasUuids;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $code
 * @property string $name
 *
 * @mixin Eloquent
 */
class Language extends Model
{
    use HasUuids;

    public $timestamps = false;

    protected $fillable = [
        'code',
        'name',
    ];
}
