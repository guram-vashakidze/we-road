<?php

namespace App\Http\Resources;

use App\Helpers\Swagger\Enums\SwaggerTypeEnum;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;
use OpenApi\Attributes as OAT;

class ErrorResource extends JsonResource
{
    public static $wrap = null;

    #[
        OAT\Schema(
            schema: 'ErrorResponse',
            title: 'Error response',
            description: 'Error response',
            required: ['error', 'success', 'code'],
            properties: [
                new OAT\Property(
                    property: 'error',
                    description: 'Error message',
                    type: SwaggerTypeEnum::STRING,
                    example: 'Error message',
                    nullable: false
                ),
                new OAT\Property(
                    property: 'success',
                    description: 'Success flag',
                    type: SwaggerTypeEnum::BOOL,
                    example: false
                ),
                new OAT\Property(
                    property: 'code',
                    description: 'Response code',
                    type: SwaggerTypeEnum::INT,
                    example: Response::HTTP_BAD_REQUEST
                ),
            ],
            type: SwaggerTypeEnum::OBJECT
        )
    ]
    public function toArray($request): array
    {
        return [
            'error' => $this->resource['error'],
            'success' => false,
            'code' => $this->resource['code'] ?? Response::HTTP_BAD_REQUEST
        ];
    }
}
