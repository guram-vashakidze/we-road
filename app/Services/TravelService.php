<?php

namespace App\Services;

use App\Dto\TravelDto;
use App\Exceptions\SomethingWentWrongException;
use App\Models\Travel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

class TravelService
{
    /**
     * @param TravelDto $dto
     * @param Travel $travel
     * @return Travel
     * @throws SomethingWentWrongException
     * @throws Throwable
     */
    public function fill(TravelDto $dto, Travel $travel): Travel
    {
        try {
            DB::beginTransaction();
            $travel
                ->setName($dto->name)
                ->setDescription($dto->description)
                ->setPublic($dto->public)
                ->setNumberOfDays($dto->numberOfDays)
                ->setMoods($dto->moods)
                ->save();
            DB::commit();
            return $travel;
        } catch (Throwable $e) {
            DB::rollBack();
            Log::error($e);
            throw new SomethingWentWrongException();
        }
    }
}
