<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        $this->call(
            [
                LanguageSeeder::class,
                RoleSeeder::class,
                UserSeeder::class,
                PasswordSeeder::class,
                TravelSeeder::class,
                TourSeeder::class
            ]
        );
    }
}
