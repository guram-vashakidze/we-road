<?php

namespace Tests\Integration\Rules;

use App\Models\Tour;
use App\Models\Travel;
use App\Rules\TourUniqRule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TourUniqRuleTest extends TestCase
{
    use DatabaseTransactions;
    use WithFaker;

    private Travel $travel;

    protected function setUp(): void
    {
        parent::setUp();
        $this->travel = Travel::factory()->create();
    }

    private function mockRequest(?Tour $tour = null): void
    {
        $this
            ->app
            ->bind(
                'request',
                fn () => new class ($this->travel, $tour) {
                    public function __construct(
                        private readonly Travel $travel,
                        private readonly ?Tour $tour
                    ) {
                    }

                    public function route(): object
                    {
                        return new class ($this->travel, $this->tour) {
                            public function __construct(
                                private readonly Travel $travel,
                                private readonly ?Tour $tour
                            ) {
                            }

                            public function parameter(string $parameter): ?Model
                            {
                                if ($parameter === 'travel') {
                                    return $this->travel;
                                }
                                return $this->tour;
                            }
                        };
                    }
                }
            );
    }

    public function test_passes_validation_1(): void
    {
        $this->mockRequest();
        $this->expectNotToPerformAssertions();
        (new TourUniqRule())
            ->validate(
                'name',
                $this->faker->name,
                fn (string $message) => $this->assertIsString($message)
            );
    }

    public function test_passes_validation_2(): void
    {
        $this->mockRequest();
        $this->expectNotToPerformAssertions();
        (new TourUniqRule())
            ->validate(
                'name',
                Tour::factory()->create()->name,
                fn (string $message) => $this->assertIsString($message)
            );
    }

    public function test_passes_validation_3(): void
    {
        $tour = Tour::factory()
            ->for(
                $this->travel,
                'travel'
            )
            ->create();
        $this->mockRequest($tour);
        $this->expectNotToPerformAssertions();
        (new TourUniqRule())
            ->validate(
                'name',
                $tour->name,
                fn (string $message) => $this->assertIsString($message)
            );
    }

    public function test_not_passes_validation_1(): void
    {
        $this->mockRequest();
        $tour = Tour::factory()
            ->for(
                $this->travel,
                'travel'
            )
            ->create();
        (new TourUniqRule())
            ->validate(
                'name',
                $tour->name,
                fn (string $message) => $this->assertIsString($message)
            );
    }
}
