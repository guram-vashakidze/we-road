<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Services\AuthService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function test_login(): void
    {
        $factory = User::factory();
        $user = $factory->create();
        $this
            ->postJson(
                route('auth.login'),
                [
                    'email' => $user->email,
                    'password' => $factory->getPassword()
                ]
            )
            ->assertOk()
            ->assertJsonStructure(
                [
                    'data' => [
                        'token'
                    ]
                ]
            );
        $this
            ->assertDatabaseHas(
                'oauth_access_tokens',
                [
                    'user_id' => $user->id,
                    'client_id' => DB::table('oauth_clients')
                        ->where('name', AuthService::PERSONAL_TOKEN_NAME)
                        ->value('id'),
                    'revoked' => 0
                ]
            );
    }

    public function test_logout(): void
    {
        $user = User::factory()->create();
        $token = $user->createToken(AuthService::PERSONAL_TOKEN_NAME);
        $user->withAccessToken($token->token);
        $this
            ->postJson(
                uri: route('auth.logout'),
                headers: [
                    'Authorization' => 'Bearer ' . $token->accessToken
                ]
            )
            ->assertOk();
        $this
            ->assertDatabaseHas(
                'oauth_access_tokens',
                [
                    'user_id' => $user->id,
                    'client_id' => DB::table('oauth_clients')
                        ->where('name', AuthService::PERSONAL_TOKEN_NAME)
                        ->value('id'),
                    'revoked' => 1
                ]
            );
    }

    public function test_logout_failed(): void
    {
        $this
            ->postJson(uri: route('auth.logout'))
            ->assertUnauthorized();
    }
}
