<?php

return [
    'something_went_wrong' => 'Something went wrong.',
    'uniq_travel' => 'Travel with :attribute ":value" already exists',
    'uniq_tour' => 'Tour with :attribute ":value" already exists in travel ":travel"',
    'tour_dates_interval' => 'Incorrect date interval. Travel ":travel" has :days days'
];
