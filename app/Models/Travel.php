<?php

namespace App\Models;

use App\Helpers\Model\HasUuids;
use Database\Factories\TravelFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

/**
 * @property string $id
 * @property string $slug
 * @property string $name
 * @property string $description
 * @property int $number_of_days
 * @property bool $public
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property int $mood_nature
 * @property int $mood_relax
 * @property int $mood_history
 * @property int $mood_culture
 * @property int $mood_party
 *
 * @see self::moods()
 * @property array $moods
 *
 * @see self::tours()
 * @property Tour[]|Collection<Tour> $tours
 *
 * @method static TravelFactory factory($count = null, $state = [])
 */
class Travel extends Model
{
    use HasFactory;
    use HasUuids;

    protected $table = 'travels';

    protected $fillable = [
        'slug',
        'name',
        'description',
        'number_of_days',
        'public',
    ];

    protected $hidden = [
        'mood_nature',
        'mood_relax',
        'mood_history',
        'mood_culture',
        'mood_party',
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'public' => 'bool'
    ];

    protected $appends = [
        'moods'
    ];

    protected function moods(): Attribute
    {
        return Attribute::make(
            get: static fn (mixed $value, array $data): array => [
                'nature' => $data['mood_nature'],
                'relax' => $data['mood_relax'],
                'history' => $data['mood_history'],
                'culture' => $data['mood_culture'],
                'party' => $data['mood_party'],
            ],
            set: static function (array $moods, array $columns): array {
                $columns['mood_nature'] = $moods['nature'];
                $columns['mood_relax'] = $moods['relax'];
                $columns['mood_history'] = $moods['history'];
                $columns['mood_culture'] = $moods['culture'];
                $columns['mood_party'] = $moods['party'];
                return $columns;
            }
        );
    }

    /**
     * @param $query
     * @param $value
     * @param $field
     * @return Relation|Builder
     */
    public function resolveRouteBindingQuery($query, $value, $field = null): Relation|Builder
    {
        if (Str::isUuid($value)) {
            return parent::resolveRouteBindingQuery($query, $value, 'id');
        }
        return parent::resolveRouteBindingQuery($query, $value, 'slug');
    }

    public function tours(): HasMany|Tour
    {
        return $this->hasMany(Tour::class, 'travel_id', 'id');
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        $this->slug = Str::slug($name);
        return $this;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;
        return $this;
    }

    public function setNumberOfDays(int $numberOfDays): self
    {
        $this->number_of_days = $numberOfDays;
        return $this;
    }

    public function setPublic(bool $public): self
    {
        $this->public = $public;
        return $this;
    }

    public function setMoods(array $moods): self
    {
        $this->moods = $moods;
        return $this;
    }
}
