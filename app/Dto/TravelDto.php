<?php

namespace App\Dto;

/**
 * @property-read string $name
 * @property-read string|null $description
 * @property-read int $numberOfDays
 * @property-read bool $public
 * @property-read array $moods
 */
class TravelDto extends BaseDto
{
    protected string $name;
    protected ?string $description;
    protected int $numberOfDays;
    protected bool $public;
    protected array $moods;

    public static function init(array $args): self
    {
        $dto = new self();
        $dto->name = $args['name'];
        $dto->description = $args['description'] ?? null;
        $dto->numberOfDays = (int) $args['number_of_days'];
        $dto->public = $args['public'] ?? true;
        $dto->moods = $args['moods'];
        return $dto;
    }
}
