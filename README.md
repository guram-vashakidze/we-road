## Setup application
```
cp .env.example .env
cp .env.testing.example .env.testing
docker-compose up -d
```
After that you will need to add next record into `/etc/hosts` file:
```
127.0.0.1       we_road.loc www.we_road.loc
```
Now application is ready (all necessary dependencies, migrations, etc. are executed automatically when the `php` container starts. It take ~15 sec)

## API documentation
After `php` and `apache` containers will be ready documentation will be available in two types:
- [Swagger IO](http://we_road.loc/api/documentation)
- [Redocly](http://we_road.loc/redoc)

There are same info and different interface

## Postman collection
Postman collection was added into this repo: [WeRoad.postman_collection.json](WeRoad.postman_collection.json)

## Run tests
```
docker-compose exec php php artisan test
```

## Check PHP Coding Standards
There is setting file `.php-cs-fixer.php`
```
docker run --rm -it -w=/app -v ${PWD}:/app oskarstark/php-cs-fixer-ga:3.26.0 --diff --dry-run
```

## Notes
- Using [spatie/laravel-permission](https://spatie.be/index.php/docs/laravel-permission/v6/introduction) for access control is the better solution in real projects. Here I implement custom `Middleware` for access control
- Travel `slug` generated automatically by Travel name
- There isn't info about `public` flag in Travel object. I guess that not public Travel should not be accessible on public endpoint. But if you authorized all Travels will be accessible
- Two extra endpoints were added into application, that have ability to see Tour and Travel models
- `snake_case` was used in table's name and table's column names (instead of `camelCase`)
- Not sure is it necessary to add `ending_date` into Tour creating request. There is `starting_date` and `number_of_days` (in Travel model) - it's enough.
