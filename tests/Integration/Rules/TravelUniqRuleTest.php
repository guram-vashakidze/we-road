<?php

namespace Tests\Integration\Rules;

use App\Models\Travel;
use App\Rules\TravelUniqRule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TravelUniqRuleTest extends TestCase
{
    use DatabaseTransactions;
    use WithFaker;

    private function mockRequest(?Travel $travel = null): void
    {
        $this
            ->app
            ->bind(
                'request',
                fn () => new class ($travel) {
                    public function __construct(private readonly ?Travel $travel)
                    {
                    }

                    public function route(): object
                    {
                        return new class ($this->travel) {
                            public function __construct(private readonly ?Travel $travel)
                            {
                            }

                            public function parameter(string $parameter): ?Model
                            {
                                return $this->travel;
                            }
                        };
                    }
                }
            );
    }

    public function test_passes_validation_1(): void
    {
        $this->mockRequest();
        $this->expectNotToPerformAssertions();
        (new TravelUniqRule())
            ->validate(
                'name',
                $this->faker->name,
                fn (string $message) => $this->assertIsString($message)
            );
    }

    public function test_passes_validation_2(): void
    {
        $travel = Travel::factory()->create();
        $this->mockRequest($travel);
        $this->expectNotToPerformAssertions();
        (new TravelUniqRule())
            ->validate(
                'name',
                $travel->name,
                fn (string $message) => $this->assertIsString($message)
            );
    }

    public function test_not_passes_validation_1(): void
    {
        $this->mockRequest();
        (new TravelUniqRule())
            ->validate(
                'name',
                Travel::factory()->create()->name,
                fn (string $message) => $this->assertIsString($message)
            );
    }

    public function test_not_passes_validation_2(): void
    {
        $travel = Travel::factory()->create();
        $this->mockRequest($travel);
        (new TravelUniqRule())
            ->validate(
                'name',
                Travel::factory()->create()->name,
                fn (string $message) => $this->assertIsString($message)
            );
    }
}
