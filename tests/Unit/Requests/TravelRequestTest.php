<?php

namespace Tests\Unit\Requests;

use App\Http\Requests\TravelRequest;
use Faker\Factory;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Validator;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use Tests\CreatesApplication;

class TravelRequestTest extends TestCase
{
    use WithFaker;
    use CreatesApplication;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setUpFaker();
        $this->createApplication();
    }

    /**
     * @param array $body
     * @param bool $passes
     * @param array|null $fields
     * @return void
     * @throws Exception
     * @dataProvider data
     */
    public function test_validation_cases(array $body, bool $passes, ?array $fields = null): void
    {
        $request = TravelRequest::create(
            Request::METHOD_POST,
            $this->faker->url,
            $body
        );
        $validator = new Validator(
            $this->createMock(Translator::class),
            $request->all(),
            $request->rules()
        );
        $this->assertEquals($passes, $validator->passes());
        if ($fields === null) {
            return;
        }
        $this->assertEquals(
            $fields,
            array_keys($validator->errors()->toArray())
        );
    }

    public static function data(): array
    {
        $faker = Factory::create();
        return [
            [
                [
                    'name' => $faker->name,
                    'description' => $faker->sentence,
                    'number_of_days' => 7,
                    'public' => true,
                    'moods' => [
                        'nature' => 10,
                        'relax' => 10,
                        'history' => 10,
                        'culture' => 10,
                        'party' => 10
                    ]
                ],
                true
            ],
            [
                [
                    'name' => $faker->name,
                    'description' => $faker->sentence,
                    'number_of_days' => 0,
                    'public' => true,
                    'moods' => [
                        'nature' => 10,
                        'relax' => 10,
                        'history' => 10,
                        'culture' => 10,
                        'party' => 10
                    ]
                ],
                false,
                [
                    'number_of_days'
                ]
            ],
            [
                [
                    'name' => $faker->name,
                    'description' => $faker->sentence,
                    'number_of_days' => 1,
                    'public' => $faker->word,
                    'moods' => $faker->word
                ],
                false,
                [
                    'public',
                    'moods',
                    'moods.nature',
                    'moods.relax',
                    'moods.history',
                    'moods.culture',
                    'moods.party'
                ]
            ],
            [
                [
                    'name' => $faker->name,
                    'description' => $faker->sentence,
                    'number_of_days' => 1,
                    'public' => true,
                    'moods' => [
                        'nature' => -1,
                        'relax' => 10,
                        'history' => 10,
                        'culture' => 10,
                        'party' => 10
                    ]
                ],
                false,
                [
                    'moods.nature'
                ]
            ],
        ];
    }
}
