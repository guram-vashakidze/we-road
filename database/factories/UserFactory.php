<?php

namespace Database\Factories;

use App\Enums\RoleEnum;
use App\Models\Language;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

/**
 * @extends Factory<User>
 *
 * @method User|User[]|Collection<User> create($attributes = [], ?Model $parent = null)
 * @method User|User[]|Collection<User> make($attributes = [], ?Model $parent = null)
 */
class UserFactory extends Factory
{
    protected $model = User::class;
    private string $password;

    public function definition(): array
    {
        $this->password = $this->password ?? $this->faker->password;
        return [
            'email' => $this->faker->unique()->email,
            'password' => Hash::make($this->password),
            'role_id' => Role::where('name', RoleEnum::ADMIN->value)->first()->id,
            'language_id' => Language::inRandomOrder()->first()->id
        ];
    }

    public function getPassword(): string
    {
        $this->password = $this->password ?? $this->faker->password;
        return $this->password;
    }

    public function editor(): self
    {
        return $this
            ->state(
                [
                    'role_id' => Role::where('name', RoleEnum::EDITOR->value)->first()->id,
                ]
            );
    }
}
