<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create(
            'users',
            static function (Blueprint $table): void {
                $table
                    ->uuid('id')
                    ->primary();
                $table
                    ->string('email')
                    ->unique();
                $table
                    ->string('password', 1000);
                $table
                    ->foreignUuid('language_id')
                    ->nullable()
                    ->references('id')
                    ->on('languages')
                    ->nullOnDelete()
                    ->cascadeOnUpdate();
                $table
                    ->foreignUuid('role_id')
                    ->references('id')
                    ->on('roles');
                $table
                    ->timestamps();
            }
        );
    }

    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
