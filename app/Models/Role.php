<?php

namespace App\Models;

use App\Enums\RoleEnum;
use App\Helpers\Model\HasUuids;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string $id
 * @property RoleEnum $name
 *
 * @see self::users()
 * @property User[]|Collection<User> $users
 *
 * @mixin Eloquent
 */
class Role extends Model
{
    use HasUuids;

    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    protected $casts = [
        'name' => RoleEnum::class
    ];

    public function users(): HasMany|User
    {
        return $this->hasMany(User::class, 'role_id', 'id');
    }
}
